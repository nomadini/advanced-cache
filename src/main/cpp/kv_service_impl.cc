//
// Created by Mahmoud Taabodi on 1/28/20.
//

#include "kv_service_impl.h"
#include "util/util.h"
#include "kv.grpc.pb.h"

KVServiceImpl::KVServiceImpl(
        std::shared_ptr<mm::ExpireLRUCacheWrapper<std::string, std::shared_ptr<::kv::DenormalizedData>>> cache,
        std::shared_ptr<IdentityAerospikeService> identityAeroService) {
    this->cache = cache;
    this->identityAeroService = identityAeroService;
}

::grpc::Status KVServiceImpl::GetKV(
        ::grpc::ServerContext *context,
        const ::kv::GetKVRequest *request,
        ::kv::GetKVReply *response) {
    std::cout << "Serving request " << std::endl;
//    createExampleResponse(respone);
//    denormalizedData->set_connected_id("abc");

    auto optionalData = cache->get(request->key());
    if (optionalData == boost::none) {
        //data is not in cache
        optionalData = identityAeroService->getDenormalizedData(request->key());
        cache->put(request->key(), *optionalData);
//        std::cout<<"request->key() : "
//        << request->key() <<" , fresh optionalData connected_id"<<
//        (*optionalData)->connected_id()<<std::endl;
    }
    // data is in cache
//    std::cout<<"optionalData connected_id"<< (*optionalData)->connected_id()<<std::endl;
    auto newResponseData = new ::kv::DenormalizedData();
    newResponseData->CopyFrom(*(*optionalData).get());
    response->set_allocated_value(newResponseData);
    response->set_key(request->key());


    //message DeviceData {
    //    optional string device_id = 1;
    //    optional string geo_location = 3;
    //    optional string id_type = 4;
    //    optional bool optout = 2;
    //    optional int32 probability = 5;
    //    optional string source = 6;
    //    optional int64 ttl = 7;
    //}

    // message DenormalizedData {
    //     optional string connected_id = 1;
    //     repeated DeviceData deterministic = 2;
    //     repeated DeviceData probabilistic = 3;
    //     repeated string opted_out_ids = 4;
    // }
    return ::grpc::Status(::grpc::StatusCode::OK, "");
}