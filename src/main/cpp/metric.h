
#include "prometheus/counter.h"
#include "prometheus/gauge.h"
#include "prometheus/histogram.h"
#include "prometheus/registry.h"
#include "prometheus/summary.h"

namespace mm {


    class MetricHolder {


        MetricHolder() {

            const std::string name = "some_name";
            const std::string help = "Additional description.";
            const std::map <std::string, std::string> const_labels = {{"key", "value"}};
            const std::map <std::string, std::string> more_labels = {{"name", "test"}};
            // const std::vector<ClientMetric::Label> expected_labels = getExpectedLabels();

            prometheus::Histogram histogram{{1, 2}};
            histogram.ObserveMultiple({5, 9, 3}, 20);
            histogram.ObserveMultiple({0, 20, 6}, 34);
            auto metric = histogram.Collect();
            auto h = metric.histogram;
        }

    };
}
