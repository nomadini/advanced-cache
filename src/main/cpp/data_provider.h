
#include <memory>
#include <string>
#include <vector>
#include "kv.grpc.pb.h"
#include "atomic_long.h"

namespace mm {

class DataProvider {

    private:

    public:
        std::shared_ptr<AtomicLong> numberOfMessages;
        DataProvider();
        DataProvider(DataProvider const &) = delete;
        DataProvider(DataProvider&&) = delete;

       ~DataProvider();

    std::shared_ptr<std::vector <std::shared_ptr<::kv::DenormalizedData>>> createExampleResponse(int sampleSize);
    };
}