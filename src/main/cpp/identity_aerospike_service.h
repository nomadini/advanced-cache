//
// Created by Mahmoud Taabodi on 2/24/20.
//

#ifndef ADVANCED_CACHE_IDENTITY_AEROSPIKE_SERVICE_H
#define ADVANCED_CACHE_IDENTITY_AEROSPIKE_SERVICE_H

#include <string>
#include <memory>
#include "aerospike_driver.h"
#include "kv.pb.h"

class IdentityAerospikeService {
private:
    // Names for Aerospike bins; string values must be under 15 chars
    static std::string createdOn;
    // We're indexing on `associds`, but that's not done by Go code right now;
    // if that index name ever changes, we'll have to update it here.
    static std::string accociatedIDs;

    std::shared_ptr <AerospikeDriver> driver;
    std::string namespaceName;
    std::string setName;
public:
    static std::string blobData;

    std::shared_ptr<::kv::DenormalizedData> getDenormalizedDataUsingFilter(std::string uuid);

    std::shared_ptr<::kv::DenormalizedData> getDenormalizedData(std::string uuid);

    std::shared_ptr<::kv::DenormalizedData> getDenormalizedDataAsync(std::string uuid);


    IdentityAerospikeService(std::shared_ptr <AerospikeDriver> driver,
                             std::string namespaceName,
                             std::string setName);

    virtual ~IdentityAerospikeService();
};


#endif //ADVANCED_CACHE_IDENTITY_AEROSPIKE_SERVICE_H
