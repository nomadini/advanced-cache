//
// Created by Mahmoud Taabodi on 2/24/20.
//
#include "util/util.h"
#include "identity_aerospike_service.h"

std::string IdentityAerospikeService::blobData = "binstruct";
std::string IdentityAerospikeService::createdOn = "created";
std::string IdentityAerospikeService::accociatedIDs = "associds";

IdentityAerospikeService::IdentityAerospikeService(
        std::shared_ptr <AerospikeDriver> driver,
        std::string namespaceName,
        std::string setName) {
    this->driver = driver;
    this->namespaceName = namespaceName;
    this->setName = setName;
}

std::shared_ptr<::kv::DenormalizedData> IdentityAerospikeService::getDenormalizedData(std::string uuid) {
    int cacheResultStatus = -1;
    auto bytesInString = driver->getKV(namespaceName, setName, uuid, IdentityAerospikeService::blobData, cacheResultStatus);
    auto data = std::make_shared<::kv::DenormalizedData>();
    bool result = (*data).ParseFromString(bytesInString);
    if (result) {
        VLOG(1) << "read data properly from db : "<< data->connected_id();
    }
    return data;
}

std::shared_ptr<::kv::DenormalizedData> IdentityAerospikeService::getDenormalizedDataUsingFilter(std::string uuid) {
    auto bytesInString = driver->findListWithContent(namespaceName, setName, uuid, IdentityAerospikeService::accociatedIDs);
    auto data = std::make_shared<::kv::DenormalizedData>();
    bool result = data->ParseFromString(bytesInString);
    if (result) {
        VLOG(1) << "read data properly from db : "<< data->connected_id();
    }
    return data;
}

std::shared_ptr<::kv::DenormalizedData> IdentityAerospikeService::getDenormalizedDataAsync(std::string uuid) {
    auto bytesInString = driver->findListWithContent(namespaceName, setName, uuid, IdentityAerospikeService::accociatedIDs);
    auto data = std::make_shared<::kv::DenormalizedData>();
    bool result = (*data).ParseFromString(bytesInString);
    if (result) {
        VLOG(1) << "read data properly from db";
    }
    return data;
}

IdentityAerospikeService::~IdentityAerospikeService() {

}