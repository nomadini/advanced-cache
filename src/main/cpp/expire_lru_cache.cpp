#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "Poco/Exception.h"
#include "Poco/ExpireLRUCache.h"
#include "Poco/Bugcheck.h"
#include "Poco/Delegate.h"
#include "expire_lru_cache.h"
#include "expire_lru_cache.h"
#include "util/util.h"
#include "kv.pb.h"

// class EntityToModuleStateStats;
template<class K, class V>
mm::ExpireLRUCacheWrapper<K, V>::ExpireLRUCacheWrapper(
        int cacheSize,
        int cacheExpiryInMinutes,
        std::string entityName) {
    Poco::Timestamp::TimeDiff expireTimeInMilliSec = cacheExpiryInMinutes * 60 * 1000;
    internalCache =
            std::make_shared < Poco::ExpireLRUCache < K, V >> (
            cacheSize,
                    expireTimeInMilliSec
    );
    this->entityName = entityName;
}

template<class K, class V>
mm::ExpireLRUCacheWrapper<K, V>::~ExpireLRUCacheWrapper() {

}

template<class K, class V>
bool mm::ExpireLRUCacheWrapper<K, V>::containKey(K key) {
    return internalCache->has(key);
}

template<class K, class V>
boost::optional <V> mm::ExpireLRUCacheWrapper<K, V>::get(K key) {

    if (containKey(key)) {
        boost::optional <V> value = *(internalCache->get(key));
        // entityToModuleStateStats->
        // addStateModuleForEntity("CACHE_HIT",
        //                         "ExpireLRUCacheWrapper-" +entityName,
        //                         "ALL");

        return value;
    }
    // entityToModuleStateStats->
    // addStateModuleForEntity("CACHE_MISS",
    //                         "ExpireLRUCacheWrapper-" + entityName,
    //                         "ALL");

    return boost::none;
}

template<class K, class V>
void mm::ExpireLRUCacheWrapper<K, V>::put(K key, V value) {
    return internalCache->add(key, value);
}

template<class K, class V>
void mm::ExpireLRUCacheWrapper<K, V>::remove(K key) {
    return internalCache->remove(key);
}


template class mm::ExpireLRUCacheWrapper<std::string, std::shared_ptr<::kv::DenormalizedData>>;