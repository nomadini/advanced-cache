//
// Created by Mahmoud Taabodi on 1/28/20.
//



#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include "util.h"
#include "string_util.h"
#include "kv.grpc.pb.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;


class KvClient {
public:
    KvClient(std::shared_ptr <Channel> channel)
            : stub_(kv::KVService::NewStub(channel)) {
    }

    bool getKV(int numberOfMessages) {
        ClientContext context;

        kv::GetKVRequest request;
        auto key = std::string("device") + StringUtil::toStr(numberOfMessages);
        request.set_key(key);
        kv::GetKVReply response;
        Status status = stub_->GetKV(&context, request, &response);
        if (!status.ok()) {
            std::cout << "GetKV rpc failed." << std::endl;
            return false;
        }
        auto val = response.value();
        std::cout << "Server returns connected_id : " << val.connected_id() << std::endl;
        for (int i = 0; i < val.deterministic_size(); i++) {
            auto device = val.deterministic(i);
            std::cout << "device_id : " << device.device_id() << std::endl;
            std::cout << "id_type : " << device.id_type() << std::endl;
            std::cout << "optout : " << device.optout() << std::endl;
            std::cout << "probability : " << device.probability() << std::endl;
            std::cout << "source : " << device.source() << std::endl;
            std::cout << "ttl : " << device.ttl() << std::endl;
            std::cout << "geo_location : " << device.geo_location() << std::endl;
        }
        for (int i = 0; i < val.probabilistic_size(); i++) {
            auto device = val.probabilistic(i);
            std::cout << "device_id : " << device.device_id() << std::endl;
            std::cout << "id_type : " << device.id_type() << std::endl;
            std::cout << "optout : " << device.optout() << std::endl;
            std::cout << "probability : " << device.probability() << std::endl;
            std::cout << "source : " << device.source() << std::endl;
            std::cout << "ttl : " << device.ttl() << std::endl;
            std::cout << "geo_location : " << device.geo_location() << std::endl;
        }
        for (int i = 0; i < val.opted_out_ids_size(); i++) {
            auto id = val.opted_out_ids(i);
            std::cout << "opted_out_id : " << id << std::endl;
        }

        std::cout << "Server returns connected_id : " << val.connected_id() << std::endl;
        std::cout << "key : " << response.key() << std::endl;
        std::cout << "created_on : " << response.created_on() << std::endl;
        return true;
    }

    std::unique_ptr <kv::KVService::Stub> stub_;
};

int main(int argc, char **argv) {
    // Expect only arg: --db_path=path/to/route_guide_db.json.
//    std::string db = routeguide::GetDbFileContent(argc, argv);
    KvClient client(
            grpc::CreateChannel("localhost:50051",
                                grpc::InsecureChannelCredentials()));

    for (int i = 0; i < 100000; i++) {
        client.getKV(i);
    }

    return 0;
}
