//
// Created by Mahmoud Taabodi on 2/22/20.
//

#ifndef ADVANCED_CACHE_DATETIMEUTIL_H
#define ADVANCED_CACHE_DATETIMEUTIL_H
#include "boost/date_time/posix_time/posix_time.hpp"
using namespace boost::posix_time;
typedef boost::posix_time::ptime Time;
typedef unsigned long long TimeType;

class DateTimeUtil {
public:
    static TimeType getNowInSecond() { // returns the epoch time in seconds
        Time time_t_epoch (boost::gregorian::date (1970, 1, 1));
        Time now = microsec_clock::universal_time ();
        time_duration diff = now - time_t_epoch;
        TimeType x = diff.total_seconds ();
        return x;
    }
};


#endif //ADVANCED_CACHE_DATETIMEUTIL_H
