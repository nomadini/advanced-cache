#ifndef ConcurrentHashMap_h
#define ConcurrentHashMap_h

#include <atomic>
#include <memory>
#include <string>
#include <vector>
#include <tbb/concurrent_hash_map.h>

#include "ConcurrentCacheTraits.h"
#include <unordered_map>

namespace mm {

/*
   General Documentation :
   https://software.intel.com/en-us/node/506191
   https://www.threadingbuildingblocks.org/docs/doxygen/annotated.html

   performance notes
   0! Specify the initial size of the table as an appropriate known level.
   It scales bad with default initial size.1. Check the hash function.
   It should take into account the fact that the table size is power of two
   and it requires randomness in the lowest bits of the hash value, e.g.
   multiply it by a prime number (see the Reference).2. Do not use accessors where possible -
   they are effectively locks,
   e.g. rewritecontainsKey() as { return _ds.count(key); }
   3. Use tbbmalloc


   //  tbb::concurrent_hash_map<std::string, tbb::concurrent_hash_map<std::string> >::iterator iter;
   //  for (iter = map.map->begin ();
   //       iter != map.map->end ();
   //       iter++) {


   auto map = getLogsToStatusMap()->getCopyOfMap();
   typename tbb::concurrent_hash_map<K, V >::iterator iter;
   for (auto iter = map->begin ();
        iter != map->end ();
        iter++) {
           LOG(INFO) << "log name : " << iter->first
                     << ", level : " << iter->second;
   }

 */
    class AtomicBoolean;

    class AtomicLong;

    template<class K, class V>
    class ConcurrentHashMap {
    private:

        boost::shared_mutex _access;

        std::shared_ptr <tbb::concurrent_hash_map<K, std::shared_ptr < V>> >
        map;
        std::shared_ptr <std::unordered_map<K, std::shared_ptr < V>> >
        stdMap;
    public:

        typedef ConcurrentCacheTraits<K, V> CacheTraits;

        std::shared_ptr <mm::AtomicBoolean> getMapType() {
            // true means we are using std::unordered_map
            static auto mode = std::make_shared<mm::AtomicBoolean>(true);
            return mode;
        }

        ConcurrentHashMap() {
            map = std::make_shared < tbb::concurrent_hash_map < K, std::shared_ptr < V > > > ();
            if (getMapType()) {
                stdMap = std::make_shared < std::unordered_map < K, std::shared_ptr < V > > > ();
            }
        }

        virtual ~ConcurrentHashMap() {
            if (getMapType()->getValue()) {
                stdMap->clear();
            } else {
                map->clear();
            }

        }


        void put(K key, std::shared_ptr <V> value) {
            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);

            if (getMapType()->getValue()) {
                stdMap->insert(std::make_pair(key, value));
            } else {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;
                map->insert(accessor, key);
                accessor->second = value;
            }
        }

        void putIfNotExisting(K key, std::shared_ptr <V> value) {
            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);
            if (getMapType()->getValue()) {
                auto pairPtr = stdMap->find(key);
                if (pairPtr == stdMap->end()) {
                    stdMap->insert(std::make_pair(key, value));
                }

            } else {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;
                if (!map->find(accessor, key)) {
                    map->insert(accessor, key);
                    accessor->second = value;
                }
            }

        }

        std::shared_ptr <V> get(K key) {
            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);
            if (getMapType()->getValue()) {
                auto pairPtr = stdMap->find(key);
                if (pairPtr != stdMap->end()) {
                    return pairPtr->second;
                }
            } else {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;
                if (map->find(accessor, key)) {
                    std::shared_ptr <V> value = accessor->second;
                    return value;
                }

            }


            throwEx("value was not found for key ");
        }

        std::shared_ptr <V> getOptional(K key) {
            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);
            std::shared_ptr <V> retValue;
            if (getMapType()->getValue()) {
                auto pairPtr = stdMap->find(key);
                if (pairPtr != stdMap->end()) {
                    retValue = pairPtr->second;
                }
            } else {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;
                std::shared_ptr <V> retValue;
                if (map->find(accessor, key)) {
                    retValue = accessor->second;
                }

            }


            return retValue;
        }

        std::shared_ptr <V> getOrCreate(K key) {

            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);
            std::shared_ptr <V> retValue;
            if (getMapType()->getValue()) {
                auto pairPtr = stdMap->find(key);
                if (pairPtr != stdMap->end()) {
                    retValue = pairPtr->second;
                } else {
                    retValue = CacheTraits::getNewInstance();
                    NULL_CHECK(retValue);
                    stdMap->insert(std::make_pair(key, retValue));
                }
            } else {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;

                if (map->find(accessor, key)) {
                    retValue = accessor->second;
                } else {
                    retValue = CacheTraits::getNewInstance();
                    NULL_CHECK(retValue);
                    map->insert(accessor, key);
                    accessor->second = retValue;
                }

            }

            return retValue;
        }

        std::vector <K> getKeys() {
            std::vector <K> keys;
            try {
                // get upgradable access
                boost::upgrade_lock <boost::shared_mutex> lock(_access);

                // get exclusive access
                boost::upgrade_to_unique_lock <boost::shared_mutex> uniqueLock(lock);
                // now we have exclusive access

                if (getMapType()->getValue()) {
                    for (auto &&pairs : *stdMap) {
                        keys.push_back(pairs.first);
                    }
                } else {

                    typename tbb::concurrent_hash_map<K, V>::iterator iter;
                    for (auto iter = map->begin();
                         iter != map->end();
                         iter++) {
                        keys.push_back(iter->first);
                    }
                }
            } catch (...) {
                mm::Util::showStackTrace();
            }

            return keys;
        }

        std::vector <std::shared_ptr<V>> getValues() {
            std::vector <std::shared_ptr<V>> values;
            try {
                // get upgradable access
                boost::upgrade_lock <boost::shared_mutex> lock(_access);

                // get exclusive access
                boost::upgrade_to_unique_lock <boost::shared_mutex> uniqueLock(lock);
                // now we have exclusive access

                if (getMapType()->getValue()) {
                    for (auto &&pairs : *stdMap) {
                        values.push_back(pairs.second);
                    }
                } else {
                    typename tbb::concurrent_hash_map<K, V>::iterator iter;
                    for (auto iter = map->begin();
                         iter != map->end();
                         iter++) {
                        values.push_back(iter->second);
                    }
                }
            } catch (...) {
                mm::Util::showStackTrace();
            }


            return values;
        }

        bool empty() {
            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);

            bool result = false;
            if (getMapType()->getValue()) {
                result = stdMap->empty();
            } else {
                result = map->empty();
            }


            return result;
        }

        bool exists(K key) {

            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);

            if (getMapType()->getValue()) {
                auto pairPtr = stdMap->find(key);
                if (pairPtr != stdMap->end()) {
                    return true;
                }
            } else {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;
                if (map->find(accessor, key)) {
                    return true;
                }
            }

            return false;
        }

        long size() {
            // get shared access
            boost::shared_lock <boost::shared_mutex> lock(_access);

            long size = 0;
            if (getMapType()->getValue()) {
                size = (long) stdMap->size();
            } else {
                size = (long) map->size();
            }
            return size;
        }

        std::shared_ptr <tbb::concurrent_hash_map<K, std::shared_ptr < V>> >

        getCopyOfMap() {
            try {
                // get upgradable access
                boost::upgrade_lock <boost::shared_mutex> lock(_access);

                // get exclusive access
                boost::upgrade_to_unique_lock <boost::shared_mutex> uniqueLock(lock);
                // now we have exclusive access

                return getCopyMapUnsafe();

            } catch (...) {
                mm::Util::showStackTrace();
            }
            throwEx("error in getting a copy of map");
        }

        std::shared_ptr <tbb::concurrent_hash_map<K, std::shared_ptr < V>> >

        moveMap() {
            try {
                // get upgradable access
                boost::upgrade_lock <boost::shared_mutex> lock(_access);

                // get exclusive access
                boost::upgrade_to_unique_lock <boost::shared_mutex> uniqueLock(lock);
                // now we have exclusive access

                auto copyOfMap = getCopyMapUnsafe();
                //we clear the original map while we have the lock
                if (getMapType()->getValue()) {
                    stdMap->clear();
                } else {
                    map->clear();
                }


                return copyOfMap;

            } catch (...) {
                mm::Util::showStackTrace();
            }
            throwEx("error in getting a copy of map");
        }

        void clear() {
            //clear method for noraml tbb map is not thread safe,
            //here we get a write lock to clear the method,
            //in all other methods, we are getting shared access lock

            try {
                // get upgradable access
                boost::upgrade_lock <boost::shared_mutex> lock(_access);

                // get exclusive access
                boost::upgrade_to_unique_lock <boost::shared_mutex> uniqueLock(lock);
                // now we have exclusive access

                if (getMapType()->getValue()) {
                    stdMap->clear();
                } else {
                    map->clear();
                }
            } catch (...) {
                mm::Util::showStackTrace();
            }

        }

        std::shared_ptr <tbb::concurrent_hash_map<K, std::shared_ptr < V>> >

        getCopyMapUnsafe() {
            if (getMapType()->getValue()) {
                //concurrent_hash_map is empty , we fill it up first then we return copy of map
                for (auto &&pairs : *stdMap) {
                    typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                    accessor;
                    map->insert(accessor, pairs.first);
                    accessor->second = pairs.second;
                }
            }

            auto copyOfMap = std::make_shared < tbb::concurrent_hash_map < K, std::shared_ptr<V> > >();
            typename tbb::concurrent_hash_map<K, V>::iterator iter;
            for (auto iter = map->begin();
                 iter != map->end();
                 iter++) {
                typename tbb::concurrent_hash_map<K, std::shared_ptr < V> > ::accessor
                accessor;
                copyOfMap->insert(accessor, iter->first);
                accessor->second = iter->second;
            }
            return copyOfMap;
        }

    };

}

#endif