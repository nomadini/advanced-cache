#ifndef SIGNAL_HANDLER_H
#define SIGNAL_HANDLER_H

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

class SignalHandler {
public :

    static void signal_callback_handler(int signum);
    static void installHandlers();

};

#endif
