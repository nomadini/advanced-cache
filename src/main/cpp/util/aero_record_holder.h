#ifndef COMMON_AeroRecordHolder_H
#define COMMON_AeroRecordHolder_H

#include <aerospike/as_record.h>

class AeroRecordHolder {
public:
    as_record *record;

    AeroRecordHolder();

    virtual ~AeroRecordHolder();
};

#endif
