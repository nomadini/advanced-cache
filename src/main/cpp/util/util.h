//
// Created by Mahmoud Taabodi on 1/27/20.
//

#ifndef ADVANCED_CACHE_UTIL_H
#define ADVANCED_CACHE_UTIL_H

#include <string>

#define UNW_LOCAL_ONLY

#include <libunwind.h>
#include <memory>
#include "glog/logging.h"
#include "LogLevelManager.h"
#include <boost/lexical_cast.hpp>
#include <iostream>

#define _F_ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define _L_ (toStrJustForUtil(" ")+toStrJustForUtil(_F_)+toStrJustForUtil(":")+toStrJustForUtil(":")+toStrJustForUtil(__LINE__)+toStrJustForUtil("_")+toStrJustForUtil(__func__))
#define _L1_ (toStrJustForUtil(_F_)+toStrJustForUtil(":")+toStrJustForUtil(":")+toStrJustForUtil(__LINE__)+toStrJustForUtil("_")+toStrJustForUtil(__func__))
#define assertAndThrow(argv) mm::Util::assertAndThrow_(_L1_, argv, #argv);
#define throwEx(args ...) mm::Util::throwEx_(_L1_, args);
#define EXIT(args ...) mm::Util::_exit(_L1_, args);

template<typename T>
extern std::string toStrJustForUtil(T i) {
    std::string str;
    try {
        str = boost::lexical_cast<std::string>(i);
    } catch (boost::bad_lexical_cast &e) {
        std::cout << "bad cast happening in converting to toStrJustForUtil";
    }
    return str;
}

namespace mm {

    class Util {
    public:
        template<typename T>
        static std::string to_str(T i);
        static void _exit(std::string caller, std::string msg);
        static void throwEx_(const std::string &caller, const std::string &msg, bool loud = false);

        static void showStackTrace(std::exception const *e = nullptr);

        static void configureLogging(std::string appName, char *argv[]);

        static int getProcessId();

        static void customTerminate();

        static void sleepMillis(int millis);

        static void assertAndThrow_(const std::string &caller,
                                    bool resultOfOperation,
                                    const std::string &operation);
    };

}
#endif //ADVANCED_CACHE_UTIL_H

