#include "AtomicBoolean.h"

mm::AtomicBoolean::AtomicBoolean() {
    booleanValue = std::make_shared < std::atomic < bool > > ();
    booleanValue->store(false, std::memory_order_seq_cst);
}

mm::AtomicBoolean::AtomicBoolean(bool value) {
    booleanValue = std::make_shared < std::atomic < bool > > ();
    booleanValue->store(value, std::memory_order_seq_cst);
}

bool mm::AtomicBoolean::getValue() {
    return booleanValue->load();
}

void mm::AtomicBoolean::setValue(bool val) {
    booleanValue->store(val, std::memory_order_seq_cst);

}

void mm::AtomicBoolean::alternate() {
    auto current = booleanValue->load();
    while (!booleanValue->compare_exchange_weak(current, !current));
}
