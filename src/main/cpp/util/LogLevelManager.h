//
// Created by Mahmoud Taabodi on 2/15/16.
//

#ifndef BIDDER_LOGLEVELMANAGER_H
#define BIDDER_LOGLEVELMANAGER_H


#include <memory>
#include <string>
#include <memory>
#include <vector>
#include "AtomicBoolean.h"

namespace mm {
    template<class K, class V>
    class ConcurrentHashMap;
}

class StringHolder;

template<class K, class V>
class ConcurrentHashMap;

#define MLOG(verboselevel) if (LogLevelManager::logOrNot(_F_)) LOG(INFO)
#define MLOG_EVERY_N(verboselevel, everyNtimes) if (LogLevelManager::logOrNot(_F_)) LOG_EVERY_N(verboselevel, everyNtimes)

class LogLevelManager {

public:

    std::shared_ptr <mm::ConcurrentHashMap<std::string, StringHolder>> mapOfFileNamesToLogStatus;
    bool allLogsAreOn;
    bool allLoggingIsTurnedOff;

    std::shared_ptr <mm::AtomicBoolean> stopThread;
    std::shared_ptr <mm::AtomicBoolean> threadIsRunning;

    std::string getLogsToStatusMapAsJson();

    std::shared_ptr <mm::ConcurrentHashMap<std::string, StringHolder>> getLogsToStatusMap();


    static bool logOrNot(std::string fileName);

    void disable(std::string fileName);

    void enableAll();

    void enableOnly(std::string fileName);

    void disableAll();

    void printAllLogLevels();

    void enableLogThread(std::string appPropertyFileName);

    std::vector <std::string> arguments;

    LogLevelManager(std::vector <std::string> all,
                    bool allLogsAreOn,
                    bool allLoggingIsTurnedOff);

    static void startLogEnablerThread(std::string appPropertyFileName);

    static std::shared_ptr <LogLevelManager> shared_instance();

    virtual ~LogLevelManager();

private:

};

#endif //BIDDER_LOGLEVELMANAGER_H
