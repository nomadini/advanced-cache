
#include "util.h"
#include "string_util.h"
#include <boost/any.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/random/random_device.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include "Poco/StringTokenizer.h"
#include <cppconn/sqlstring.h>
#include <boost/algorithm/hex.hpp>
#include <boost/functional/hash.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/regex.hpp>

#include <boost/algorithm/string/predicate.hpp>
#include <random>

using boost::lexical_cast;
using boost::bad_lexical_cast;

#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>
#include <cstring>
#include <openssl/sha.h>


boost::random::random_device StringUtil::randomDevice;

StringUtil::StringUtil() {

}

std::string StringUtil::joinArrayAsString(std::vector <std::string> elemens, std::string delimiter) {
    return boost::algorithm::join(elemens, delimiter);
}

std::string StringUtil::getDifference(std::string oldStr, std::string newStr) {
    // LOG(ERROR) << " oldStr : " << oldStr;
    // LOG(ERROR) << " newStr : " << oldStr;
    std::string diff;
    std::string stringWithoutFirstDiff;
    bool foundDiff = false;
    for (int i = 0; i < oldStr.size(); i++) {
        char oldChar = oldStr.at(i);
        char newChar = ' ';
        if (newStr.size() > i) {
            newChar = newStr.at(i);
        }
        std::string newDiff;
        if (oldChar != newChar) {
            newDiff = _toStr(oldChar) + " vs " + _toStr(newChar) + " ,";
            foundDiff = true;
        } else {
            if (!foundDiff) {
                stringWithoutFirstDiff += oldChar;
            }
            newDiff = _toStr(oldChar) + " == " + _toStr(newChar) + " ,";
        }
        diff += newDiff;

    }

    //print the string in newStr that is not part of oldStr, because of bigger size
    if (newStr.size() > oldStr.size()) {
        int i = oldStr.size() - 1;
        for (int i = 0; i < newStr.size(); i++) {
            char oldChar = ' ';
            char newChar = newStr.at(i);
            auto newDiff = _toStr(oldChar) + " vs " + _toStr(newChar) + " ,";
            diff += newDiff;
        }
    }

    if (foundDiff) {
        // LOG(ERROR) << "first good portion : " << stringWithoutFirstDiff;
    }
    return diff;
}

bool StringUtil::matches(std::string str, std::string regex) {
    boost::regex expr{regex};
    return boost::regex_match(str, expr);
}

std::vector <std::string> StringUtil::searchWithRegex(std::string str, std::string regex) {
    boost::regex expr{regex};
    boost::smatch what;

    std::vector <std::string> matchedSubstrings;
    if (boost::regex_search(str, what, expr)) {
        for (unsigned int i = 0; i < what.size(); i++) {
            VLOG(10) << "WHAT : " << i << " " << what[i] << std::endl;
            matchedSubstrings.push_back(what[i]);
        }
    }

    return matchedSubstrings;
}

/**
 * this function is case insensitive
 */
bool StringUtil::contains(const std::string &mainString, const std::string &partialString) {

    /**
     * contians is case sensitive vs icontains
     */
    return boost::algorithm::contains(mainString, partialString);
}


/**
 * this function is case sensitive
 */
bool StringUtil::containsCaseSensitive(const std::string &mainString, const std::string &partialString) {

    /**
     * contians is case sensitive vs icontains
     */
    return boost::algorithm::icontains(mainString, partialString);

}

/**
 * this function is case insensitive
 */
bool StringUtil::containsCaseInSensitive(const std::string &mainString, const std::string &partialString) {

    /**
     * contians is case sensitive vs icontains
     */
    return boost::algorithm::icontains(mainString, partialString);
}

bool StringUtil::equalsIgnoreCase(const std::string &foo, const std::string &bar) {
    return boost::iequals(foo, bar);
}

bool StringUtil::equalsCaseSensitive(const std::string &foo, const std::string &bar) {
    return boost::equals(foo, bar);
}

template<typename T>
std::string StringUtil::toStr(T i) {
    std::string str;
    try {
        str = boost::lexical_cast<std::string>(i);
    } catch (boost::bad_lexical_cast &e) {
        throwEx("bad cast happening in converting to toStr");
    }
    return str;
}

std::string StringUtil::toStrFromAny(boost::any sth) {
    const std::type_info &ti = sth.type();
    std::string type = StringUtil::toStr(ti.name());
    std::string str;
    try {
        if (type.compare("s") == 0 ||
            type.compare("Ss") == 0) {
            str = boost::any_cast<std::string>(sth);
        } else if (type.compare("PKc") == 0
                   || type.compare("const char *") == 0) {
            str = StringUtil::toStr(boost::any_cast<const char *>(sth));
        } else if (type.compare("f") == 0) {
            str = StringUtil::toStr(boost::any_cast<float>(sth));
        } else if (type.compare("i") == 0) {
            str = StringUtil::toStr(boost::any_cast<int>(sth));
        } else if (type.compare("d") == 0) {
            str = StringUtil::toStr(boost::any_cast<double>(sth));
        } else if (type.compare("l") == 0) {
            str = StringUtil::toStr(boost::any_cast<long>(sth));
        }
        if (type.compare("b") == 0) {
            str = StringUtil::toStr(boost::any_cast<bool>(sth));
        }
    } catch (boost::bad_any_cast &e) {
        LOG(ERROR) << "StringUtil: toStrFromAny : exception throw in casting the type " <<
                   StringUtil::toStr(e.what());
    }

    if (str.empty()) {
        LOG(ERROR) << "StringUtil: toStrFromAny : type is " << type;
        throw new std::runtime_error("exception : StringUtil: toStrFromAny : type is " + type + "is unknown ");
    }
    return str;
}

/**
 * no empty token is returned, and it works on delimiters being a word
 */
std::vector <std::string> StringUtil::tokenizeString(std::string tokens,
                                                     const std::string &delimiter) {
    size_t pos = 0;
    // VLOG(3) << "tokens is : " << tokens;
    std::vector <std::string> allTokens;
    while ((pos = tokens.find(delimiter)) != std::string::npos) {
        // VLOG(3) << "pos is : " << pos;
        std::string token = tokens.substr(0, pos);
        // VLOG(3) << "token is : " << token;
        if (!token.empty()) {
            allTokens.push_back(token);
        }
        tokens.erase(0, pos + delimiter.length());
    }
    allTokens.push_back(tokens);//put the remainder of sentence back
    return allTokens;
}

/**
 * works only for characters
 */
std::vector <std::string> StringUtil::split(const std::string &line, const std::string &sep) {
    std::vector <std::string> strs;
    boost::split(strs, line, boost::is_any_of(sep));
    return strs;
}

/**
 * this will return a "" if the size of str is 0, this is very important for database service like EventLogCassandraService
 */
std::string StringUtil::emptyStringIfZeroSize(std::string str) {
    if (str.empty()) {
        return "";
    } else {
        return str;
    }
}

/**
 * this will return only small letters in string
 * this is good for creating an id like browserid
 */
std::string StringUtil::random_string_pure(size_t length) {
    static auto &chrs = "0123456789"
                        "abcdefghijklmnopqrstuvwxyz"
                        "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    thread_local static std::mt19937 rg{std::random_device{}()};
    thread_local static std::uniform_int_distribution <std::string::size_type>
            pick(0, sizeof(chrs) - 2);

    std::string s;

    s.reserve(length);

    while (length--)
        s += chrs[pick(rg)];

    return s;
}

std::string StringUtil::random_string(size_t length) {
    const char charset[] = "0123456789"
                           "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                           "abcdefghijklmnopqrstuvwxyz";
    const size_t max_index = 62;


    boost::random::uniform_int_distribution<> index_dist(0, max_index);

    std::string str;
    for (int i = 0; i < length; i++) {
        char ch = charset[index_dist(randomDevice)];
        if ((int) ch != 0) {
            str.append(StringUtil::toStr(ch));
        } else {
            i--; //to repeat this step!!
        }
    }
    return str;
}

std::string StringUtil::random_string(std::string firstPart, size_t length) {
    return firstPart.append(StringUtil::random_string(length));
}

//good for debugging
void StringUtil::printEveryCharacterInString(std::string str) {
    for (std::string::iterator it = str.begin(); it != str.end(); ++it) {
        VLOG(0) << "char is " << (*it) << " number is " << (int) (*it) << "\n";
    }
}

std::string StringUtil::replaceString(std::string mainString,
                                      const std::string &oldSubstring,
                                      const std::string &newSubstring,
                                      bool subStringMustExists) {

    if (subStringMustExists) {
        if (!boost::algorithm::contains(mainString, oldSubstring)) {
            LOG(ERROR) << oldSubstring << " doesnt exist in " << mainString;
            assertAndThrow(false);
        }
    }

    boost::replace_all(mainString, oldSubstring.c_str(), newSubstring);
    return mainString;

}

std::string StringUtil::replaceStringCaseInsensitive(std::string mainString,
                                                     const std::string &oldSubstring,
                                                     const std::string &newSubstring,
                                                     bool subStringMustExists) {
    if (subStringMustExists) {
        if (!boost::algorithm::contains(mainString, oldSubstring)) {
            LOG(ERROR) << oldSubstring << " doesnt exist in " << mainString;
            assertAndThrow(false);
        }
    }

    boost::ireplace_all(mainString, oldSubstring.c_str(), newSubstring);
    return mainString;
}


std::string StringUtil::string_to_hex(const std::string &input) {
    return boost::algorithm::hex(input);
}

std::string StringUtil::hex_to_string(const std::string &input) {
    return boost::algorithm::unhex(input);
}

std::string StringUtil::hashStringBySha1(std::string str) {
    unsigned char digest[SHA_DIGEST_LENGTH];

    SHA_CTX ctx;
    SHA1_Init(&ctx);
    SHA1_Update(&ctx, str.c_str(), strlen(str.c_str()));
    SHA1_Final(digest, &ctx);

    char mdString[SHA_DIGEST_LENGTH * 2 + 1];
    for (int i = 0; i < SHA_DIGEST_LENGTH; i++)
        sprintf(&mdString[i * 2], "%02x", (unsigned int) digest[i]);

    return std::string(mdString);
}

std::size_t StringUtil::getHashedNumber(std::string str) {
    boost::hash<std::string> hasher;
    return hasher(str);
}

std::string StringUtil::toLowerCase(const std::string &str) {
    return boost::algorithm::to_lower_copy(str);
}

std::string StringUtil::toUpperCase(const std::string &str) {
    return boost::algorithm::to_upper_copy(str);
}

std::string StringUtil::getLastNCharactersOfString(std::string input, int n) {
    if (input.size() < n) {
        throwEx("size of input string is less than " + StringUtil::toStr(n) + ", input : " + input);
    }
    return input.substr(input.size() - n);
}

bool StringUtil::endsWith(std::string mainString, std::string subString) {
    return boost::algorithm::ends_with(mainString.c_str(), subString.c_str());
}


template std::string StringUtil::toStr<char *>(char *);

template std::string StringUtil::toStr<unsigned long>(unsigned long);

template std::string StringUtil::toStr<unsigned long long>(unsigned long long);

template std::string StringUtil::toStr<unsigned int>(unsigned int);

template std::string StringUtil::toStr<std::string>(std::string);

template std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >
StringUtil::toStr<long long>(long long);

template std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >
StringUtil::toStr<unsigned short>(unsigned short);
