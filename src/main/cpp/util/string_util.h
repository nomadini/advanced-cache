#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <vector>
#include <string>

namespace boost {
    class any;
    namespace random {
        class random_device;
    }
}


#define _toStr(arg) StringUtil::toStr(arg)

class StringUtil {
    StringUtil();

public:
    static boost::random::random_device randomDevice;

    template<typename T>
    static std::string toStr(T i);

/**
 * compares a string with a regular expression. It will return true only if the expression matches the complete string.
 */
    static bool matches(std::string str, std::string regex);

    static std::vector <std::string> searchWithRegex(std::string str, std::string regex);

    static std::vector <std::string> split(const std::string &str, const std::string &sep);

    static std::vector <std::string> tokenizeString(std::string tokens,
                                                    const std::string &sep);

    static std::string toStrFromAny(boost::any sth);

    static std::string emptyStringIfZeroSize(std::string str);

    static std::string getDifference(std::string oldStr, std::string newStr);

    static std::string joinArrayAsString(std::vector <std::string> elemens, std::string delimiter);

/**
 * this will return only small letters in string
 * this is good for creating an id like browserid
 */
    static std::string random_string_pure(size_t length);

    static std::string random_string(size_t length);

    static std::string random_string(std::string firstPart, size_t length);

    static void printEveryCharacterInString(std::string str);

    static std::string replaceString(std::string mainString,
                                     const std::string &oldSubstring,
                                     const std::string &newSubstring,
                                     bool subStringMustExists = false);

    static std::string replaceStringCaseInsensitive(std::string mainString,
                                                    const std::string &oldSubstring,
                                                    const std::string &newSubstring,
                                                    bool subStringMustExists = false);

    static std::string string_to_hex(const std::string &input);

    static std::string hex_to_string(const std::string &input);

    static bool contains(const std::string &mainString, const std::string &partialString);

    static bool containsCaseSensitive(const std::string &mainString, const std::string &partialString);

    static bool containsCaseInSensitive(const std::string &mainString, const std::string &partialString);

    static bool equalsIgnoreCase(const std::string &foo, const std::string &bar);

    static bool equalsCaseSensitive(const std::string &foo, const std::string &bar);

    static std::string hashStringBySha1(std::string str);

    static std::size_t getHashedNumber(std::string str);

    static std::string toLowerCase(const std::string &str);

    static std::string toUpperCase(const std::string &str);

    static std::string getLastNCharactersOfString(std::string input, int n);

    static bool endsWith(std::string mainString, std::string subString);
};

#endif
