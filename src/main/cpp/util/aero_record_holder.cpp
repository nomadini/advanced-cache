

#include "aero_record_holder.h"
#include "util.h"

AeroRecordHolder::AeroRecordHolder() {
        this->record = NULL;
}

AeroRecordHolder::~AeroRecordHolder() {
        if (this->record) {
                as_record_destroy(record);
        }
}