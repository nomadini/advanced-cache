
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "signal_handler.h"
#include "util.h"


// Define the function to be called when ctrl-c (SIGINT) signal is sent to process
void SignalHandler::signal_callback_handler(int signum) {
    LOG(ERROR) << "SignalHandler : Caught signal : " << signum;
    mm::Util::showStackTrace();

    //we exit after handling a signal, to make sure program stops
    if (signum == 2) {
        LOG(ERROR) << signum << " signal causes exiting..bye";
        exit(0);
    }
}

void SignalHandler::installHandlers() {

    std::set_terminate(mm::Util::customTerminate);

    // // signal(SIGHUP, signal_callback_handler);//0 //don't catch sighup because nohup wont work anymore, since we are exiting after any signal
    // signal(SIGINT, signal_callback_handler);//1
    // signal(SIGQUIT, signal_callback_handler);//2
    // signal(SIGILL, signal_callback_handler);  //3   Terminal quit (POSIX)
    signal(SIGABRT, signal_callback_handler); //4   signal abort
    // signal(SIGILL, signal_callback_handler); //4   Illegal instruction (ANSI)
    // signal(SIGTRAP, signal_callback_handler); //5   Trace trap (POSIX)
    // signal(SIGIOT, signal_callback_handler); //6   IOT Trap (4.2 BSD)
    // signal(SIGBUS, signal_callback_handler);  //7   BUS error (4.2 BSD)
    // signal(SIGFPE, signal_callback_handler); //8   Floating point exception (ANSI)
    // signal(SIGKILL, signal_callback_handler); //9   Kill(can't be caught or ignored) (POSIX)
    // signal(SIGUSR1, signal_callback_handler);  //10  User defined signal 1 (POSIX)
    // signal(SIGSEGV, signal_callback_handler); //11  Invalid memory segment access (ANSI)
    // signal(SIGUSR2, signal_callback_handler);  //12  User defined signal 2 (POSIX)
    // signal(SIGPIPE, signal_callback_handler); //13  Write on a pipe with no reader, Broken pipe (POSIX)
    // signal(SIGALRM, signal_callback_handler); //14  Alarm clock (POSIX)
    // signal(SIGTERM, signal_callback_handler); //15  Termination (ANSI)
    signal(SIGSTKFLT, signal_callback_handler); //  16  Stack fault
    // signal(SIGCHLD, signal_callback_handler); //17  Child process has stopped or exited, changed (POSIX)
    // signal(SIGCONT, signal_callback_handler); //18  Continue executing, if stopped (POSIX)
    signal(SIGSTOP, signal_callback_handler); //19  Stop executing(can't be caught or ignored) (POSIX)
    // signal(SIGTSTP, signal_callback_handler); //20  Terminal stop signal (POSIX)
    // signal(SIGTTIN, signal_callback_handler); //21  Background process trying to read, from TTY (POSIX)
    // signal(SIGTTOU, signal_callback_handler); //22  Background process trying to write, to TTY (POSIX)
    // signal(SIGURG, signal_callback_handler); //23  Urgent condition on socket (4.2 BSD)
    signal(SIGXCPU, signal_callback_handler); //24  CPU limit exceeded (4.2 BSD)
    signal(SIGXFSZ, signal_callback_handler); //25  File size limit exceeded (4.2 BSD)
    // signal(SIGVTALRM, signal_callback_handler); //  26  Virtual alarm clock (4.2 BSD)
    // signal(SIGPROF, signal_callback_handler); //27  Profiling alarm clock (4.2 BSD)
    // signal(SIGWINCH, signal_callback_handler); //  28  Window size change (4.3 BSD, Sun)
    // signal(SIGIO, signal_callback_handler); //29  I/O now possible (4.2 BSD)
    // signal(SIGPWR, signal_callback_handler); //30  Power failure restart (System V)
}
