//
// Created by Mahmoud Taabodi on 2/22/20.
//
#include <boost/exception/diagnostic_information.hpp>
#include "file_util.h"
#include "util.h"
#include "date_time_util.h"
#include "Poco/File.h"
#include <fstream>
#include <iostream>
#include <Poco/DirectoryIterator.h>
#include <Poco/DateTimeFormatter.h>
#include <boost/filesystem.hpp>

int FileUtil::deleteFileOlderThanXMinutesInDir(const std::string &directory, int minutes) {
    auto allFiles = getAllFilesOlderThanXMinutesInDir(directory, minutes);
    int numberOfFilesDeleted = 0;
    for (auto fileName : allFiles) {
        LOG(ERROR) << "deleting file : " << fileName;
        numberOfFilesDeleted += deleteFile(fileName);
    }
    return numberOfFilesDeleted;
}

int FileUtil::deleteFile(const std::string& name) {
    assertAndThrow(!name.empty());
    return remove(name.c_str());
}

std::vector<std::string> FileUtil::getAllFilesOlderThanXMinutesInDir(const std::string &directory, int minutes) {
    std::vector<std::string> allFiles;
    Poco::DirectoryIterator end;
    long nowInSecond = DateTimeUtil::getNowInSecond();
    for (Poco::DirectoryIterator it(directory); it != end; ++it) {
        if (!it->isDirectory()) {
            long lastModifiedInMinutes = it->getLastModified().epochMicroseconds() / (1000000 * 60);
            long nowInMinutes = (nowInSecond / 60);
            if (nowInMinutes - lastModifiedInMinutes > minutes) {
                allFiles.push_back(it->path());
            }
        }

    }
    return allFiles;
}

bool FileUtil::checkIfFileExists(const std::string &name) {
    try {
        auto file = std::make_shared<Poco::File>(name);
        return file->exists();
    } catch (const std::exception &e) {
        auto diagnosticInformation = boost::diagnostic_information(e);
        LOG_EVERY_N(ERROR, 1) << "Got the " << google::COUNTER << "th exception "
                              << " while handling request : " << diagnosticInformation;
        return false;
    }
}

bool FileUtil::createDirectory(const std::string &nameOfDir) {
    VLOG(2) << "going to create directory " << nameOfDir;
    boost::filesystem::path dir(nameOfDir.c_str());
    if (boost::filesystem::create_directories(dir)) {
        VLOG(2) << nameOfDir << " directory was created";
        return true;
    } else {
        VLOG(2) << nameOfDir << " failure : directory was NOT created";
        return false;
    }
}

void FileUtil::writeStringToFile(const std::string &filename, const std::string &fileContent) {

    std::ofstream outfile;
    outfile.open(filename.c_str(), std::ios_base::out);
    outfile << fileContent;
    outfile.close();
}
