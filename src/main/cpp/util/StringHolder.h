#ifndef StringHolder_h
#define StringHolder_h


#include <atomic>
#include <memory>
#include <string>
#include "atomic_long.h"

//use AtomicLong instead of this class
class StringHolder;

class StringHolder {

public:

    std::string value;

    StringHolder();

    virtual ~StringHolder();

    std::string toJson();
};

#endif
