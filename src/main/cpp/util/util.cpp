//
// Created by Mahmoud Taabodi on 1/27/20.
//
#include <chrono>
#include <thread>
#include "util.h"
#include "file_util.h"
#include "LogLevelManager.h"
#include <glog/logging.h>
#include <boost/lexical_cast.hpp>
#include <string.h>
#include <stdlib.h> //exit function
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
//#include <bfd/bfd.h>
//#include <libiberty.h>
#include <dlfcn.h>
#include <link.h>
//#include <cxxabi.h>
#include <boost/exception/diagnostic_information.hpp>
#include <boost/exception_ptr.hpp>
#include <cxxabi.h>

using boost::lexical_cast;
using boost::bad_lexical_cast;

template<typename T>
std::string mm::Util::to_str(T i) {
    std::string str;
    try {
        str = boost::lexical_cast<std::string>(i);
    } catch (boost::bad_lexical_cast &e) {
        throw std::logic_error("bad cast happening in converting to toStr");
    }
    return str;
}

void mm::Util::_exit(std::string caller, std::string msg) {
    LOG(ERROR)<< "exiting based on unacceptable condition at "<< caller<<"error message : "<<msg;
    mm::Util::showStackTrace();
    exit(1);
}

void mm::Util::sleepMillis(int millis) {
    std::chrono::milliseconds dura(millis);
    std::this_thread::sleep_for(dura);
}

void mm::Util::customTerminate() {

    LOG(ERROR) << "terminate handler called!, pay attention to thread number!";
    showStackTrace();
}

// http://blog.bigpixel.ro/2010/09/stack-unwinding-stack-trace-with-gcc/
void mm::Util::showStackTrace(std::exception const *e) {
    if (e) {
        LOG(ERROR) << " exception thrown: " << boost::diagnostic_information(*e);
    }

    char name[256];
    unw_cursor_t cursor;
    unw_context_t uc;
    unw_word_t ip, sp, offp;

    unw_getcontext(&uc);
    unw_init_local(&cursor, &uc);
    LOG(ERROR) << "---------------beginning of stacktrace---------------------";
    while (unw_step(&cursor) > 0) {
        char file[256];
        int line = 0;
        name[0] = '\0';
        unw_get_proc_name(&cursor, name, 256, &offp);
        unw_get_reg(&cursor, UNW_REG_IP, &ip);
        unw_get_reg(&cursor, UNW_REG_SP, &sp);

        int status;
        //A pointer to the start of the NUL-terminated demangled name, or NULL if the demangling fails. The caller is responsible for deallocating this memory using free.
        char *realname = abi::__cxa_demangle(name, 0, 0, &status);
        if (realname != nullptr) {
            std::shared_ptr<char> p(realname, &free);
            std::string str(p.get());
            LOG(ERROR) << str;
        } else {
            //demangling has failed!!!
            //LOG(ERROR) << "demangling failed!";
        }

    }

    LOG(ERROR) << "-----------------end of stacktrace-------------------------";
}

int mm::Util::getProcessId() {
    LOG(INFO) << "Process ID " << getpid();
    return getpid();
}

void mm::Util::assertAndThrow_(const std::string &caller,
                           bool resultOfOperation,
                           const std::string &operation) {
    if (!resultOfOperation) {
        LOG(ERROR) << "caller:  " << caller << ", result of '" << operation;
        showStackTrace();
        throw std::logic_error("result of operation is not true at " + caller + " operation : " + operation);
    }
}

void mm::Util::throwEx_(const std::string &caller, const std::string &msg, bool loud) {
    LOG_EVERY_N(ERROR, 1000) << google::COUNTER << "th caller:  " << caller << ", throwing exception : " << msg;
    if (loud) {
        //this is too much but its good to see where exception is throwing from
        //by default its off
        showStackTrace();
    }
    throw std::logic_error(msg);
}

/**
 * is used by main function of the different programs to start up the basic threads
 */
void mm::Util::configureLogging(std::string appName, char *argv[]) {

    // Poco::SignalHandler::install();

    // FLAGS_log_dir = "/var/log/";
    std::string logfileName = "/var/log/" + appName;

    FileUtil::createDirectory(logfileName);
    FileUtil::deleteFileOlderThanXMinutesInDir(logfileName, 60);

    FLAGS_log_dir = logfileName;

    FLAGS_logbufsecs = 10;
    //Log messages to stderr instead of logfiles.
    //Note: you can set binary flags to true by specifying 1, true, or yes (case insensitive). Also, you can set binary flags to false by specifying 0, false, or no (again, case insensitive).
    // FLAGS_logtostderr = false;
    FLAGS_alsologtostderr = true;

//    Log messages at or above this level. Again, the numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.

    FLAGS_max_log_size = 1000000; //this is in MB I guess
    FLAGS_stop_logging_if_full_disk = true;


    // minloglevel (int, default=0, which is INFO)
    // Log messages at or above this level. Again, the numbers of severity levels INFO, WARNING, ERROR, and FATAL are 0, 1, 2, and 3, respectively.
    FLAGS_minloglevel = 0;

    FLAGS_v = 1;
    //   VLOG(3) << "I'm printed when you run the program with --v=1 or higher";
    // VLOG(2) << "I'm printed when you run the program with --v=2 or higher";
    // v (int, default=0)
    // Show all VLOG(m) messages for m less or equal the value of this flag. Overridable by --vmodule. See the section about verbose logging for more detail.


    // FLAGS_drop_log_memory = true;
//    FLAGS_stderrthreshold = 0;
    FLAGS_colorlogtostderr = true;
//    FLAGS_minloglevel = 0;
//    FLAGS_stderrthreshold = 0;


    google::InstallFailureSignalHandler(); //this option is better than the two lines above

    //creating the lock file//this is very important to make sure that two instances of
    //process are not running
    std::string lockFileName = "/var/data/" + appName + ".lock";
    if (FileUtil::checkIfFileExists(lockFileName)) {
        // throwEx("another instance of program is running...lock file exists : " + lockFileName);
        LOG(ERROR) << "another instance of program is running...lock file exists : " + lockFileName;
    } else {
        FileUtil::writeStringToFile(lockFileName, "");
    }

    LOG(ERROR) << "initializing the logging for app : " << appName;
    google::InitGoogleLogging(appName.c_str());
    //these have to run after InitGoogleLogging is called
    LOG(ERROR) << "starting the dynamic logging thread";
    LogLevelManager::startLogEnablerThread(appName);
    LOG(ERROR) << "printing processId";
    mm::Util::getProcessId();
}

template std::string mm::Util::to_str<char *>(char *);

template std::string mm::Util::to_str<char const *>(char const *);