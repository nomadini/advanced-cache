/*
 * ConverterUtil.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_UTIL_CONVERTERUTIL_H_
#define GICAPODS_GICAPODSSERVER_SRC_UTIL_CONVERTERUTIL_H_

class StringUtil;


#include <boost/lexical_cast.hpp>
#include "string_util.h"

using boost::lexical_cast;
using boost::bad_lexical_cast;

class ConverterUtil {
    ConverterUtil();

public:

    template<typename T>
    static T convertTo(std::string sth);

    static long convertStringToLong(std::string str);

    static std::string convertLongToString(long num);

    template<typename F, typename T>
    static T TO(F sth);

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_UTIL_CONVERTERUTIL_H_ */
