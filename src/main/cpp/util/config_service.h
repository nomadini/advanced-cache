/*
 * ConfigServiceParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */

#ifndef CONFIG_H_H
#define CONFIG_H_H


#include "Poco/Util/PropertyFileConfiguration.h"
#include <memory>
#include <string>
#include <unordered_map>

namespace mm {
    class ConfigService {
    private:
        std::shared_ptr <std::unordered_map<std::string, std::string>> allLoadedProperties;

    public:

        ConfigService(std::string filePathName,
                      std::string commonPropertyFileName);


        bool propertyExists(std::string propertyName);

        std::string get(std::string propertyName);

        int getAsInt(std::string propertyName);

        long getAsLong(std::string propertyName);

        bool getAsBoolean(std::string propertyName);

        double getAsDouble(std::string propertyName);

        bool getAsBooleanFromString(std::string propertyName);

        template<typename T>
        void get(std::string propertyName, T &propertyHolder);

        void upsertProperty(std::string name, std::string value);

        std::shared_ptr <std::unordered_map<std::string, std::string>> readProperties(
                std::string filePathName,
                bool addToLoadedMap = true);

        std::shared_ptr <std::unordered_map<std::string, std::string>> getAllLoadedProperties();

        static void upsertProperty(std::shared_ptr <std::unordered_map<std::string, std::string>> allLoadedProperties,
                                   std::string name,
                                   std::string value);

        void clearAllProperties();

        void addProperties(std::shared_ptr <std::unordered_map<std::string, std::string>> propMap);

        ~ConfigService();
    };


}
#endif
