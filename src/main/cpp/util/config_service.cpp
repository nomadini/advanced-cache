/*
 * ConfigServiceParser.h
 *
 *  Created on: Mar 4, 2015
 *      Author: mtaabodi
 */


#include "util.h"
#include "config_service.h"
#include "string_util.h"
#include "file_util.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>

mm::ConfigService::ConfigService(
        std::string filePathName,
        std::string commonPropertyFileName) {
    allLoadedProperties = std::make_shared < std::unordered_map < std::string, std::string > > ();

    if (!commonPropertyFileName.empty()) {
        readProperties(commonPropertyFileName);
    } else {
        LOG(INFO) << "not loading any common property";
    }
    if (!filePathName.empty()) {
        readProperties(filePathName);
    }

}

bool mm::ConfigService::propertyExists(std::string propertyName) {
    return (allLoadedProperties->find(propertyName) != allLoadedProperties->end());
}

template<typename T>
void mm::ConfigService::get(std::string propertyName, T &propertyHolder) {
    propertyHolder = ConverterUtil::convertTo<T>(get(propertyName));
}

int mm::ConfigService::getAsInt(std::string propertyName) {
    return ConverterUtil::convertTo<int>(get(propertyName));
}

long mm::ConfigService::getAsLong(std::string propertyName) {
    return ConverterUtil::convertTo<long>(get(propertyName));
}

bool mm::ConfigService::getAsBoolean(std::string propertyName) {
    return ConverterUtil::convertTo<bool>(get(propertyName));
}

bool mm::ConfigService::getAsBooleanFromString(std::string propertyName) {
    auto prop = get(propertyName);
    if (StringUtil::equalsIgnoreCase(prop, "true")) {
        return true;
    }

    return false;
}

double mm::ConfigService::getAsDouble(std::string propertyName) {
    return ConverterUtil::convertTo<double>(get(propertyName));
}

template<>
void mm::ConfigService::get(std::string propertyName, std::string &propertyHolder) {
    propertyHolder = get(propertyName);
}

void mm::ConfigService::clearAllProperties() {
    allLoadedProperties->clear();
}

void mm::ConfigService::addProperties(std::shared_ptr <std::unordered_map<std::string, std::string>> propMap) {
    for (auto &&entryPair : *propMap) {
        upsertProperty(entryPair.first, entryPair.second);
    }
}

std::string mm::ConfigService::get(std::string propertyName) {
    MLOG(10) << "looking for property : " << propertyName;

    if (propertyExists(propertyName)) {
        return allLoadedProperties->find(propertyName)->second;
    } else {
        MLOG(10) << "printing all properties.....";
        //TODO
//        LOG(ERROR) << "all properties loaded : " << JsonMapUtil::convertMapToJsonArray(*allLoadedProperties);
        LOG(ERROR) << "property was not found : " << propertyName;
        mm::Util::showStackTrace();
        EXIT(std::string("property ") + propertyName + std::string(" doesnt exist"));
    }
}

std::shared_ptr <std::unordered_map<std::string, std::string>>
mm::ConfigService::readProperties(std::string filePathName, bool addToLoadedMap) {
    filePathName = StringUtil::toLowerCase(filePathName);
    MLOG(3) << "reading properties from file : " << filePathName;
    auto mapOfProperties =
    std::make_shared < std::unordered_map < std::string, std::string > > ();
    if (filePathName.empty()) {
        EXIT("filePathName is empty");
    }
    filePathName = StringUtil::toStr("/home/advanced-cache/src/main/resources/").append(filePathName);

    // LOG(INFO)<<"FileUtil::getCurrentDirectory() "<<FileUtil::getCurrentDirectory();

    if (!FileUtil::checkIfFileExists(filePathName)) {
        EXIT("file : " + filePathName + " doesn't exist");
    }

    Poco::AutoPtr <Poco::Util::PropertyFileConfiguration> localPropertyLoader
            = new Poco::Util::PropertyFileConfiguration();
    localPropertyLoader->load(filePathName);

    std::vector <std::string> allRootKeys;
    localPropertyLoader->keys(allRootKeys);
    MLOG(2) << "loading properties from file : " << filePathName;
    for (std::string key :  allRootKeys) {
          LOG(INFO) << " found key " << key << " in property file : "<<filePathName;

        if (localPropertyLoader->hasProperty(key)) {
            auto prop = localPropertyLoader->getString(key);
            if (addToLoadedMap) {
                upsertProperty(key, prop);
            }
            mapOfProperties->insert(std::make_pair(key, prop));
        } else {
//            EXIT("propery key  has no value, key:  " + key);
        }
    }

    return mapOfProperties;
}

std::shared_ptr <std::unordered_map<std::string, std::string>> mm::ConfigService::getAllLoadedProperties() {
    return allLoadedProperties;
}

void
mm::ConfigService::upsertProperty(std::shared_ptr <std::unordered_map<std::string, std::string>> allLoadedProperties,
                                  std::string name, std::string value) {
    //this function first removes any existing property
    auto it = allLoadedProperties->find(name);
    if (it != allLoadedProperties->end()) {
        allLoadedProperties->erase(it);
    }

    allLoadedProperties->insert(std::make_pair(name, value));
}

void mm::ConfigService::upsertProperty(std::string name, std::string value) {
    upsertProperty(allLoadedProperties, name, value);
}

mm::ConfigService::~ConfigService() {
}


template void mm::ConfigService::get<int>(std::string, int &);

template void mm::ConfigService::get<double>(std::string, double &);

template void mm::ConfigService::get<bool>(std::string, bool &);

template void mm::ConfigService::get<long>(std::string, long &);

template void mm::ConfigService::get<std::string>(std::string, std::string &);
