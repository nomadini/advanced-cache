/*
 * ConverterUtil.h
 *
 *  Created on: Aug 7, 2015
 *      Author: mtaabodi
 */

#include "ConverterUtil.h"
#include "string_util.h"
#include "util.h"

ConverterUtil::ConverterUtil() {
}

template<typename T>
T ConverterUtil::convertTo(std::string sth) {
    T str;
    try {
        str = boost::lexical_cast<T>(sth);
    } catch (boost::bad_lexical_cast &e) {
        LOG(ERROR) << "TO : bad cast happening in converting :  " << sth;
        mm::Util::showStackTrace();
        throwEx("bad cast happening in converting : " + sth);
    }
    return str;

}

long ConverterUtil::convertStringToLong(std::string str) {
    long i = atol(str.c_str());
    return i;
}

std::string ConverterUtil::convertLongToString(long num) {
    return StringUtil::toStr(num);
}

template<typename F, typename T>
T ConverterUtil::TO(F sth) {
    T str;
    try {
        str = boost::lexical_cast<T>(sth);
    } catch (boost::bad_lexical_cast &e) {
        LOG(ERROR) << "TO : bad cast happening in converting :  " << sth;
        throwEx("TO : bad cast happening in converting :  " + sth);
    }
    return str;

}

template int ConverterUtil::convertTo<int>(std::__cxx11::basic_string < char, std::char_traits < char > ,
                                           std::allocator < char > > );
template double ConverterUtil::convertTo<double>(std::__cxx11::basic_string < char, std::char_traits < char > ,
                                                 std::allocator < char > > );
template unsigned long long ConverterUtil::convertTo<unsigned long long>(std::__cxx11::basic_string < char,
                                                                         std::char_traits < char > ,
                                                                         std::allocator < char > > );
template long ConverterUtil::convertTo<long>(std::__cxx11::basic_string < char, std::char_traits < char > ,
                                             std::allocator < char > > );
template bool ConverterUtil::convertTo<bool>(std::__cxx11::basic_string < char, std::char_traits < char > ,
                                             std::allocator < char > > );
template unsigned short ConverterUtil::convertTo<unsigned short>(std::__cxx11::basic_string < char,
                                                                 std::char_traits < char > ,
                                                                 std::allocator < char > > );

template double ConverterUtil::TO<int, double>(int);

template double ConverterUtil::TO<long, double>(long);

template int ConverterUtil::TO<std::__cxx11::basic_string < char, std::char_traits < char>, std::allocator<char>
>, int>(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >);

template std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > ConverterUtil::TO<
        std::__cxx11::basic_string < char, std::char_traits < char>, std::allocator<char>
>, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >
(std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >);
