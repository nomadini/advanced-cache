#include "atomic_long.h"

mm::AtomicLong::AtomicLong() {
    count = std::make_shared < std::atomic < long > > ();
    count->store(0, std::memory_order_seq_cst);
}

mm::AtomicLong::AtomicLong(long count) {
    this->count = std::make_shared < std::atomic < long > > ();
    this->count->store(count, std::memory_order_seq_cst);
}

void mm::AtomicLong::increment() {
    (*count)++;
}

void mm::AtomicLong::decrement() {
    (*count)--;
}

long mm::AtomicLong::getValue() {
    return count->load();
}

void mm::AtomicLong::setValue(long val) {
    count->store(val, std::memory_order_seq_cst);
}

void mm::AtomicLong::addValue(long val) {

    count->fetch_add(val, std::memory_order_seq_cst);
}

void mm::AtomicLong::decrement(long val) {
    count->fetch_sub(val, std::memory_order_seq_cst);
}
