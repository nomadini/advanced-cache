//
// Created by Mahmoud Taabodi on 2/22/20.
//

#ifndef ADVANCED_CACHE_FILEUTIL_H
#define ADVANCED_CACHE_FILEUTIL_H

#include <string>
#include <memory>
#include <vector>


class FileUtil {

    FileUtil();

public:
    static void writeStringToFile(const std::string &filename, const std::string &fileContent);

    static bool createDirectory(const std::string &nameOfDir);

    static int deleteFileOlderThanXMinutesInDir(const std::string &directory, int minutes);

    static bool checkIfFileExists(const std::string &name);

    static std::vector <std::string> getAllFilesOlderThanXMinutesInDir(const std::string &directory, int minutes);

    static int deleteFile(const std::string& name);
};

#endif //ADVANCED_CACHE_FILEUTIL_H
