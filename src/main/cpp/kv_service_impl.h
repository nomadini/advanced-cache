//
// Created by Mahmoud Taabodi on 1/28/20.
//

#ifndef ADVANCED_CACHE_KVSERVICEIMPL_H
#define ADVANCED_CACHE_KVSERVICEIMPL_H

#include "kv.grpc.pb.h"
#include "expire_lru_cache.h"
#include "identity_aerospike_service.h"
#include <grpcpp/server_builder.h>
#include <grpcpp/server.h>
#include <memory>

class KVServiceImpl final : public kv::KVService::Service {
public:

    std::shared_ptr<mm::ExpireLRUCacheWrapper<std::string, std::shared_ptr<::kv::DenormalizedData>>> cache;
    std::shared_ptr<IdentityAerospikeService> identityAeroService;

    KVServiceImpl(std::shared_ptr<mm::ExpireLRUCacheWrapper<std::string, std::shared_ptr<::kv::DenormalizedData>>> cache,
                  std::shared_ptr<IdentityAerospikeService> identityAeroService);

    ::grpc::Status GetKV(
            ::grpc::ServerContext *context,
            const ::kv::GetKVRequest *request,
            ::kv::GetKVReply *response);

    void createExampleResponse(::kv::GetKVReply *response);
};


#endif //ADVANCED_CACHE_KVSERVICEIMPL_H
