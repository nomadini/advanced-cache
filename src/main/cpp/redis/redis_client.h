
#include <memory>
#include <string>
#include <vector>

#include <cpp_redis/cpp_redis>

namespace mm {

    class RedisClient {

    private:
        cpp_redis::client client;

    public:

        RedisClient();

        RedisClient(RedisClient const &) = delete;

        RedisClient(RedisClient &&) = delete;

        ~RedisClient();

        void setKVAsync(std::string key, std::string value);

        void processValuesAsync();
    };
}