
#include "redis_client.h"


#include <cpp_redis/cpp_redis>

#include <iostream>

mm::RedisClient::RedisClient() {
    //! Enable logging
    cpp_redis::active_logger = std::unique_ptr<cpp_redis::logger>(new cpp_redis::logger);

    //! High availability requires at least 2 io service workers
    cpp_redis::network::set_default_nb_workers(2);

    //! Add your sentinels by IP/Host & Port
    client.add_sentinel("127.0.0.1", 26379);

    //! Call connect with optional timeout
    //! Can put a loop around this until is_connected() returns true.
    client.connect("mymaster", [](const std::string &host, std::size_t port, cpp_redis::connect_state status) {
                       if (status == cpp_redis::connect_state::dropped) {
                           std::cout << "client disconnected from " << host << ":" << port << std::endl;
                       }
                   },
                   0, -1, 5000);
}

mm::RedisClient::~RedisClient() {

}

void mm::RedisClient::setKVAsync(std::string key, std::string value) {


// same as client.send({ "SET", "hello", "42" }, ...)
    client.set("hello", "42", [](
            cpp_redis::reply &reply
    ) {
        std::cout << "set hello 42: " << reply <<
                  std::endl;
// if (reply.is_string())
//   do_something_with_string(reply.as_string())
    });

// commands are pipelined and only sent when client.commit() is called
// client.commit();

// synchronous commit, no timeout
//client.
    client.sync_commit();
}

void mm::RedisClient::processValuesAsync() {

    while (true) {
        // same as client.send({ "DECRBY", "hello", 12 }, ...)
        client.incrby("hello", 12, [](cpp_redis::reply &reply) {
            std::cout << "incrby hello 12: " << reply << std::endl;
            // if (reply.is_integer())
            //   do_something_with_integer(reply.as_integer())
        });

        // same as client.send({ "GET", "hello" }, ...)
        client.get("hello", [](cpp_redis::reply &reply) {
            std::cout << "get hello: " << reply << std::endl;
            // if (reply.is_string())
            //   do_something_with_string(reply.as_string())
        });

        // commands are pipelined and only sent when client.commit() is called
        // client.commit();

        // synchronous commit, no timeout
        client.sync_commit();
        std::cout << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(3000));
    }
}