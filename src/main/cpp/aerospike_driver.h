//
// Created by Mahmoud Taabodi on 7/12/16.
//

#ifndef COMMON_AEROSPIKECLIENT_H
#define COMMON_AEROSPIKECLIENT_H

#include <string>
#include <memory>
#include <vector>

#include <aerospike/aerospike.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/as_error.h>
#include <aerospike/as_record.h>
#include <aerospike/as_status.h>
#include "aerospike_driver.h"
#include "util/atomic_long.h"

/*
   nice multi op examples
   http://www.aerospike.com/docs/client/c/usage/kvs/multiops.html

 */
class AerospikeDriver {

private:

public:
    static std::string getStringFromBinValue(as_record *record, const std::string& binName);

    static int CACHE_HIT_WITH_VALUE;
    static int CACHE_HIT_WITH_EMPTY;
    static int CACHE_MISS;
    static int CACHE_ERROR;
    std::string stringValueSeparator;
    aerospike as;
    as_event_loop *event_loop;

    std::shared_ptr <mm::AtomicLong> numberOfCurrentReads;
    std::shared_ptr <mm::AtomicLong> numberOfCurrentWrites;

    AerospikeDriver(std::string host, int port, const char *lua_user_path);



    std::string getKV(std::string snamespace,
                          std::string sset,
                          std::string skey,
                          std::string sbin,
                          int &cacheResultStatus);

/**
 *	ttlInSeconds : The time-to-live (expiration) of the record in seconds.
 *	There are two special values that can be set in the record TTL:
 *	(*) ZERO (defined as AS_RECORD_DEFAULT_TTL), which means that the
 *	    record will adopt the default TTL value from the namespace.
 *	(*) 0xFFFFFFFF (also, -1 in a signed 32 bit int)
 *	    (defined as AS_RECORD_NO_EXPIRE_TTL), which means that the record
 *	    will get an internal "void_time" of zero, and thus will never expire.
 *
 *	Note that the TTL value will be employed ONLY on write/update calls.
 */
    void set_cache(std::string snamespace,
                   std::string sset,
                   std::string skey,
                   std::string sbin,
                   std::string svalue,
                   int ttlInSeconds);

    void set_cache_async(std::string snamespace,
                         std::string sset,
                         std::string skey,
                         std::string sbin,
                         std::string svalue,
                         int ttlInSeconds);

    long addValueAndRead(std::string snamespace,
                         std::string sset,
                         std::string skey,
                         std::string sbin,
                         int valueToAdd,
                         int ttlInSeconds);

    std::string addValueAndRead(std::string snamespace,
                                std::string sset,
                                std::string skey,
                                std::string sbin,
                                std::string valueToAdd,
                                int ttlInSeconds);

    std::string readTheBin(std::string snamespace,
                           std::string sset,
                           std::string skey,
                           std::string sbin);

    void removeRecordWithKey(std::string key,
                             std::string namespaceName,
                             std::string set);

    std::string setValueAndRead(std::string snamespace,
                                std::string sset,
                                std::string skey,
                                std::string sbin,
                                std::string valueToAdd,
                                int ttlInSeconds);

    std::string findListWithContent(std::string namespaceName,
                                              std::string set,
                                              std::string key,
                                              std::string binName);

    void findListWithContentAsync(std::string namespaceName,
                                  std::string set,
                                  std::string key,
                                  std::string binName);

//    bool query_listener(as_error *err, as_record *record, void *udata, as_event_loop *event_loop);

//    void example_dump_bin(const as_bin *p_bin);

//    void example_dump_record(const as_record *p_rec);

    virtual ~AerospikeDriver();

};


#endif //COMMON_AEROSPIKECLIENT_H
