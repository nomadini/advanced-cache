//
// Created by Mahmoud Taabodi on 7/12/16.
//
#include "util/util.h"
#include "util/string_util.h"
#include <boost/foreach.hpp>
#include "aerospike_driver.h"
#include "util/aero_record_holder.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <aerospike/as_monitor.h>
#include <aerospike/aerospike.h>
#include <aerospike/aerospike_index.h>
#include <aerospike/aerospike_key.h>
#include <aerospike/aerospike_udf.h>
#include <aerospike/aerospike_query.h>
#include <aerospike/as_bin.h>
#include <aerospike/as_bytes.h>
#include <aerospike/as_error.h>
#include <aerospike/as_config.h>
#include <aerospike/as_key.h>
#include <aerospike/as_operations.h>
#include <aerospike/as_password.h>
#include <aerospike/as_record.h>
#include <aerospike/as_record_iterator.h>
#include <aerospike/as_status.h>
#include <aerospike/as_string.h>
#include <aerospike/as_val.h>
#include <aerospike/as_query.h>

int AerospikeDriver::CACHE_HIT_WITH_VALUE = 500;
int AerospikeDriver::CACHE_HIT_WITH_EMPTY = 501;
int AerospikeDriver::CACHE_MISS = 502;
int AerospikeDriver::CACHE_ERROR = 503;
static as_monitor monitor;

static void example_dump_bin(const as_bin *p_bin) {
    if (!p_bin) {
        VLOG(1) << "  null as_bin object";
        return;
    }

    char *val_as_str = as_val_tostring(as_bin_get_value(p_bin));

    VLOG(1) << as_bin_get_name(p_bin) << " :" << val_as_str;

    free(val_as_str);
}

static void example_dump_record(const as_record *p_rec) {
    if (!p_rec) {
        VLOG(1) << "  null as_record object";
        return;
    }

    if (p_rec->key.valuep) {
        char *key_val_as_str = as_val_tostring(p_rec->key.valuep);

        VLOG(1) << "  key: " << key_val_as_str;

        free(key_val_as_str);
    }

    uint16_t num_bins = as_record_numbins(p_rec);

    VLOG(1) << "  generation " << p_rec->gen << ", ttl " << p_rec->ttl << ", %u bin%s" << num_bins;

    as_record_iterator it;
    as_record_iterator_init(&it, p_rec);

    while (as_record_iterator_has_next(&it)) {
        example_dump_bin(as_record_iterator_next(&it));
    }

    as_record_iterator_destroy(&it);
}

static bool query_listener(as_error *err, as_record *record, void *udata, as_event_loop *event_loop) {
    if (err) {
        LOG(ERROR) << "aerospike_query_async() returned " << err->code << " - " << err->message;
        as_monitor_notify(&monitor);
        return false;
    }

    if (!record) {
        VLOG(1) << "query is complete";
        as_monitor_notify(&monitor);
        return false;
    }

    VLOG(1) << "query callback returned record:";
    example_dump_record(record);
    return true;
}

AerospikeDriver::AerospikeDriver(std::string host, int port, const char *lua_user_path) {

    numberOfCurrentReads = std::make_shared<mm::AtomicLong>();
    numberOfCurrentWrites = std::make_shared<mm::AtomicLong>();

    stringValueSeparator = "^;^;^;";

    int g_port = port;
    auto g_host = host.c_str();


    auto g_user = "";
    auto g_password = "";

    // Initialize logging.
//        as_log_set_callback(example_log_callback);


    // Initialize cluster configuration.
    as_config cfg;
    as_config_init(&cfg);
    as_config_add_host(&cfg, g_host, g_port);
    as_config_set_user(&cfg, g_user, g_password);

    aerospike_init(&as, &cfg);

    as_error err;

    // Connect to the Aerospike database cluster. Assume this is the first thing
    // done after calling example_get_opts(), so it's ok to exit on failure.
    if (aerospike_connect(&as, &err) != AEROSPIKE_OK) {
        LOG(ERROR) << "aerospike_connect() returned " << err.code << " - " << err.message;
        aerospike_destroy(&as);
        throwEx("error connecting to aerospike");
    } else {
        LOG(INFO) << "aerospike successful connection established";
    }
    //TODO fix this later
//    this->event_loop = as_event_loop_get();
}

void AerospikeDriver::findListWithContentAsync(std::string namespaceName,
                                               std::string set,
                                               std::string key,
                                               std::string binName) {
    as_query query;
    as_query_init(&query, namespaceName.c_str(), set.c_str());

    as_query_where_init(&query, 1);
    as_query_where(&query, binName.c_str(), as_contains(LIST, STRING, key.c_str()));



    // Execute the query.
    as_error err;
    if (aerospike_query_async(&as, &err, NULL, &query, query_listener, NULL, this->event_loop) != AEROSPIKE_OK) {
        query_listener(&err, NULL, NULL, event_loop);
    }

    as_query_destroy(&query);
}

bool readBytesBinCallback(const as_val *val, void *udata) {
    as_record *rec = as_record_fromval(val);
    if (!rec) {
        return false;
    }
    fprintf(stderr, "record contains %d bins", as_record_numbins(rec));
    auto stringPtr = static_cast<std::string *>(udata);
    //TODO for now this is hardcoded, change it to variable name later
    //TODO check if this is correct
    *stringPtr = AerospikeDriver::getStringFromBinValue(rec, "associds");

    return true;
}

//*record will be freed by recordHolder that calls this
std::string AerospikeDriver::getStringFromBinValue(as_record *record, const std::string &binName) {

    //TODO check if we can free value after creating std::string
    char *value = as_record_get_str(record, binName.c_str());
    std::string valueStr;
    if (value != NULL) {
        valueStr = StringUtil::toStr(value);
    }

    return valueStr;
}

std::string AerospikeDriver::findListWithContent(std::string namespaceName,
                                                 std::string set,
                                                 std::string key,
                                                 std::string binName) {
    as_query query;
    as_query_init(&query, namespaceName.c_str(), set.c_str());

    as_query_where_init(&query, 1);
    as_query_where(&query, binName.c_str(), as_contains(LIST, STRING, key.c_str()));


    std::shared_ptr <std::string> stringSharedPtr = std::make_shared<std::string>();
    void *allBytesPtr = static_cast<void *>(stringSharedPtr.get());

    as_error err;
    // Execute the query.
    if (aerospike_query_foreach(&as, &err, NULL, &query,
                                readBytesBinCallback, allBytesPtr) != AEROSPIKE_OK) {
        fprintf(stderr, "error(%d) %s at [%s:%d]", err.code, err.message, err.file, err.line);
    }

    as_query_destroy(&query);
    return *stringSharedPtr;
}

void AerospikeDriver::removeRecordWithKey(std::string key, std::string namespaceName, std::string set) {

    as_key g_key;
    // Initialize the test as_key object. We won't need to destroy it since it
    // isn't being created on the heap or with an external as_key_value.
    as_key_init_str(&g_key, namespaceName.c_str(), set.c_str(), key.c_str());

    as_error err;

    // Try to remove the test record from the database. If the example has not
    // inserted the record, or it has already been removed, this call will
    // return as_status AEROSPIKE_ERR_RECORD_NOT_FOUND - which we just ignore.
    aerospike_key_remove(&as, &err, NULL, &g_key);
}

AerospikeDriver::~AerospikeDriver() {
    as_error err;
    LOG(INFO) << "Disconnecting from the aerospike database cluster and clean up the aerospike object.";

    aerospike_close(&as, &err);
    aerospike_destroy(&as);
}

std::string AerospikeDriver::getKV(std::string snamespace,
                                   std::string sset,
                                   std::string skey,
                                   std::string sbin,
                                   int &cacheResultStatus) {

    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.empty());
    assertAndThrow(!sbin.size() < 14);
    numberOfCurrentReads->increment();
    as_error err;

    //Initialize Key
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    //Read from database
    AeroRecordHolder aeroRecordHolder;
    const char *select[] = {sbin.c_str(), NULL};
    as_status statusOfRead = aerospike_key_select(&as, &err, NULL, &key, select, &aeroRecordHolder.record);
    if (statusOfRead != AEROSPIKE_OK && statusOfRead != AEROSPIKE_ERR_RECORD_NOT_FOUND) {
        LOG(ERROR) << "aerospike_key_select returned " << err.code << " - " << err.message;
        //add counter
        //        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_READING_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
        cacheResultStatus = CACHE_ERROR;
        return "";
    }


    numberOfCurrentReads->decrement();
    assertAndThrow(numberOfCurrentReads->getValue() < 1000);

    if (statusOfRead != AEROSPIKE_ERR_RECORD_NOT_FOUND) {
        cacheResultStatus = CACHE_HIT_WITH_VALUE;
        std::string value = getStringFromBinValue(aeroRecordHolder.record, sbin);
//        LOG(ERROR) << "aerospike returned "<< value;
        return value;
    } else {
        cacheResultStatus = CACHE_MISS;
//        LOG(ERROR) << "aerospike_key_select returned nothing " << err.code << " - " << err.message;
        return "";
    }
}

void AerospikeDriver::set_cache(std::string snamespace,
                                std::string sset,
                                std::string skey,
                                std::string sbin,
                                std::string svalue,
                                int ttlInSeconds) {

    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.empty());
    assertAndThrow(!sbin.size() < 14);
    //sometimes we want value to be ""
    //so that we can cache the result of not finding a data for a key
    // assertAndThrow(!svalue.empty());
    assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                    ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

    assertAndThrow(ttlInSeconds >= 10);
    as_error err;

    //Initialize Key
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    //Initialize Record Data
    as_record record;
    as_record_inita(&record, 1);
    as_record_set_str(&record, sbin.c_str(), svalue.c_str());
    record.ttl = ttlInSeconds;

    //Write to Database
    as_status status = aerospike_key_put(&as, &err, NULL, &key, &record);
    as_record_destroy(&record);
    if (status != AEROSPIKE_OK) {
//        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_PUTTING_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
        throwEx ("writing value to cache " + StringUtil::toStr(err.message));
    }
}

void asyncOpListener(as_error *err, void *udata, as_event_loop *event_loop) {
    if (err) {
        // printf("Command failed: %d %s\n", err->code, err->message);
        return;
    }
    // printf("Command succeeded\n");
}


long AerospikeDriver::addValueAndRead(std::string snamespace,
                                      std::string sset,
                                      std::string skey,
                                      std::string sbin,
                                      int valueToAdd,
                                      int ttlInSeconds) {
    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.size() < 14);
    assertAndThrow(!sbin.empty());
    assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                    ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

    numberOfCurrentReads->increment();
    long valueToReturn = 0;

    as_operations ops;

    as_operations_inita(&ops, 2);//number Of operations
    as_operations_add_incr(&ops, sbin.c_str(), valueToAdd);
    as_operations_add_read(&ops, sbin.c_str());
    ops.ttl = ttlInSeconds; ///tthis is in second
    //for reference
    // as_operations_add_append_str(&ops, "bin2", "def");

    AeroRecordHolder rec;
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    as_error err;
    if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {

//        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_ADD_READ_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
    } else {
        // printf("valueToReturn = %ld\n", as_record_get_int64(rec.record, sbin.c_str(), 0));
        valueToReturn = (long) as_record_get_int64(rec.record, sbin.c_str(), 0);
    }

    as_operations_destroy(&ops);

    numberOfCurrentReads->decrement();
    assertAndThrow(numberOfCurrentReads->getValue() < 1000);
    return valueToReturn;
}

std::string AerospikeDriver::readTheBin(std::string snamespace,
                                        std::string sset,
                                        std::string skey,
                                        std::string sbin) {
    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.empty());
    assertAndThrow(!sbin.size() < 14);
    numberOfCurrentReads->increment();
    std::string valueToReturn;

    as_operations ops;

    as_operations_inita(&ops, 1);                                  //number Of operations
    as_operations_add_read(&ops, sbin.c_str());

    AeroRecordHolder rec;
    as_error err;
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {

//        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_ADD_READ_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
    } else {
        valueToReturn = getStringFromBinValue(rec.record, sbin);
    }

    as_operations_destroy(&ops);

    numberOfCurrentReads->decrement();
    assertAndThrow(numberOfCurrentReads->getValue() < 1000);
    return valueToReturn;

}

std::string AerospikeDriver::addValueAndRead(std::string snamespace,
                                             std::string sset,
                                             std::string skey,
                                             std::string sbin,
                                             std::string valueToAdd,
                                             int ttlInSeconds) {
    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.empty());
    assertAndThrow(!sbin.size() < 14);
    assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                    ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

    std::string valueToReturn;
    numberOfCurrentReads->increment();
    as_operations ops;

    as_operations_inita(&ops, 2);//number Of operations
    valueToAdd += stringValueSeparator;
    as_operations_add_append_str(&ops, sbin.c_str(), valueToAdd.c_str());
    as_operations_add_read(&ops, sbin.c_str());
    ops.ttl = ttlInSeconds; ///tthis is in second
    //for reference
    // as_operations_add_append_str(&ops, "bin2", "def");

    AeroRecordHolder rec;
    as_error err;
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {
        //printf("error(%d) %s at [%s:%d]", err.code, err.message, err.file, err.line);
//        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_ADD_READ_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
    } else {
        valueToReturn = getStringFromBinValue(rec.record, sbin);
    }

    as_operations_destroy(&ops);

    numberOfCurrentReads->decrement();
    assertAndThrow(numberOfCurrentReads->getValue() < 1000);

    return valueToReturn;
}

std::string AerospikeDriver::setValueAndRead(std::string snamespace,
                                             std::string sset,
                                             std::string skey,
                                             std::string sbin,
                                             std::string valueToAdd,
                                             int ttlInSeconds) {
    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.empty());
    assertAndThrow(!sbin.size() < 14);
    assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                    ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

    numberOfCurrentReads->increment();
    std::string valueToReturn;

    as_operations ops;

    as_operations_inita(&ops, 2);//number Of operations
    // valueToAdd += stringValueSeparator;
    as_operations_add_write_str(&ops, sbin.c_str(), valueToAdd.c_str());
    as_operations_add_read(&ops, sbin.c_str());
    ops.ttl = ttlInSeconds; ///tthis is in second
    //for reference
    // as_operations_add_append_str(&ops, "bin2", "def");

    AeroRecordHolder rec;
    as_error err;
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    if (aerospike_key_operate(&as, &err, NULL, &key, &ops, &rec.record) != AEROSPIKE_OK) {
        //printf("error(%d) %s at [%s:%d]", err.code, err.message, err.file, err.line);
//        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_ADD_READ_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
    } else {
        valueToReturn = getStringFromBinValue(rec.record, sbin);
    }

    as_operations_destroy(&ops);
    numberOfCurrentReads->decrement();
    assertAndThrow(numberOfCurrentReads->getValue() < 1000);

    return valueToReturn;

}

void AerospikeDriver::set_cache_async(std::string snamespace,
                                      std::string sset,
                                      std::string skey,
                                      std::string sbin,
                                      std::string svalue,
                                      int ttlInSeconds) {
    assertAndThrow(!snamespace.empty());
    assertAndThrow(!sset.empty());
    assertAndThrow(!skey.empty());
    assertAndThrow(!sbin.empty());
    assertAndThrow (ttlInSeconds != AS_RECORD_DEFAULT_TTL &&
                    ttlInSeconds != AS_RECORD_NO_EXPIRE_TTL);

    assertAndThrow(!sbin.size() < 14);
    as_error err;

    //Initialize Key
    as_key key;
    as_key_init(&key, snamespace.c_str(), sset.c_str(), skey.c_str());

    //Initialize Record Data
    as_record record;
    as_record_inita(&record, 1);
    as_record_set_str(&record, sbin.c_str(), svalue.c_str());

    //for reference
    // as_record_set_str(&rec, "bin1", "abc");
    // as_record_set_int64(&rec, "bin2", 123);

    record.ttl = ttlInSeconds;

    //Write to Database
    as_status status = aerospike_key_put_async(&as, &err, NULL, &key, &record, asyncOpListener, NULL, NULL, NULL);
    as_record_destroy(&record);

    if (status != AEROSPIKE_OK) {
        //fprintf(stderr, "err(%d) %s at [%s:%d]\n", err.code, err.message, err.file, err.line);
//        entityToModuleStateStats->addStateModuleForEntity(
//                "ERROR_IN_PUT_VALUE_ASYNC_VALUE",
//                "AerospikeDriver",
//                EntityToModuleStateStats::all,
//                EntityToModuleStateStats::exception);
        throwEx ("writing value to cache " + StringUtil::toStr(err.message));
    }
}
