#include <memory>
#include <string>
#include <vector>
#include "data_provider.h"
#include "aerospike_driver.h"

namespace mm {

    class DataPopulateService {

    private:
        std::shared_ptr <DataProvider> dataProvider;
        std::shared_ptr <AerospikeDriver> aerospikeDriver;
        std::string namespaceName;
        std::string setName;
    public:
        void populateAersoSpike();

        void startPopulationThread();

        DataPopulateService(std::shared_ptr <DataProvider> dataProvider,
                            std::shared_ptr <AerospikeDriver> aerospikeDriver,
                            std::string namespaceName,
                            std::string setName);


        DataPopulateService(DataPopulateService const &) = delete;

        DataPopulateService(DataPopulateService &&) = delete;

        ~DataPopulateService();


    };
}