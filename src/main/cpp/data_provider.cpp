
#include "data_provider.h"
#include "string_util.h"
#include "util/string_util.h"

mm::DataProvider::DataProvider() {
    numberOfMessages = std::make_shared<AtomicLong>();
}

mm::DataProvider::~DataProvider() {

}

std::shared_ptr<std::vector <std::shared_ptr<::kv::DenormalizedData>>> mm::DataProvider::createExampleResponse(int sampleSize) {
    auto allData =
            std::make_shared<std::vector <std::shared_ptr<::kv::DenormalizedData>>>();


    for (int i = 0; i < sampleSize; i++) {
        auto denormalizedData = std::make_shared<::kv::DenormalizedData>();
        //StringUtil::random_string(12)
        this->numberOfMessages->increment();
        long value = this->numberOfMessages->getValue();
        std::cout<<"value : "<< value<<std::endl;
        std::string connectedIdKey = std::string("device") + StringUtil::toStr(value);
        denormalizedData->set_connected_id(connectedIdKey);

        for (int i = 0; i < 10; i++) {
            denormalizedData->add_opted_out_ids(StringUtil::random_string(12));
            denormalizedData->add_opted_out_ids(StringUtil::random_string(12));
            auto det1 = denormalizedData->add_deterministic();
            det1->set_device_id(StringUtil::random_string(12));
            det1->set_geo_location(StringUtil::random_string(12));
            det1->set_id_type("MOBILE");
            det1->set_optout(true);
            det1->set_probability(12.34);
            det1->set_ttl(140);
            det1->set_source("comcast");

            auto prob1 = denormalizedData->add_probabilistic();
            prob1->set_device_id(StringUtil::random_string(12));
            prob1->set_geo_location(StringUtil::random_string(12));
            prob1->set_id_type("mobile");
            prob1->set_optout(true);
            prob1->set_probability(12.45);
            prob1->set_ttl(143420);
            prob1->set_source("comcast");
        }
        allData->push_back(denormalizedData);
    }
    return allData;
}
