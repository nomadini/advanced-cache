#ifndef ExpireLRUCacheWrapper_H
#define ExpireLRUCacheWrapper_H


#include <memory>
#include <string>
#include <boost/optional.hpp>
#include "Poco/Exception.h"
#include "Poco/ExpireLRUCache.h"
#include "Poco/Bugcheck.h"
#include "Poco/Delegate.h"
// #include "EntityToModuleStateStats.h"

// class EntityToModuleStateStats;

namespace mm {

    template<class K, class V>
    class ExpireLRUCacheWrapper {

    private:

    public:
        // EntityToModuleStateStats* entityToModuleStateStats;
        std::string entityName;
        std::shared_ptr <Poco::ExpireLRUCache<K, V>> internalCache;

        ExpireLRUCacheWrapper<K, V>(
                int cacheSize,
                int cacheExpiryInMinutes,
                std::string entityName);

        virtual ~ExpireLRUCacheWrapper<K, V>();

        bool containKey(K key);

        boost::optional <V> get(K key);

        void put(K key, V value);

        void remove(K key);
    };
}

#endif
