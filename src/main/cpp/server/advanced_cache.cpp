
#include "expire_lru_cache.h"
#include "metric.h"
#include "kv.grpc.pb.h"

#include "kv_service_impl.h"
#include "signal_handler.h"
#include "util.h"
#include "string_util.h"
#include "data_populate_service.h"
#include "config_service.h"
#include <memory>
#include <grpcpp/server_builder.h>
#include <grpcpp/server.h>
#include <glog/logging.h>

using namespace grpc;


void RunServer(std::shared_ptr<AerospikeDriver> aerospikeDriver, std::string namespaceName, std::string setName) {
    std::string server_address("0.0.0.0:50051");

    int cacheSize = 1000;
    int cacheExpiryInMinutes = 30;
    std::string entityName = "kv_data";
    auto cache =
            std::make_shared < mm::ExpireLRUCacheWrapper < std::string,
    std::shared_ptr < ::kv::DenormalizedData >> > (cacheSize, cacheExpiryInMinutes, entityName);


    auto identityAeroService = std::make_shared<IdentityAerospikeService>(
            aerospikeDriver,
            namespaceName,
            setName
            );
    auto service = std::make_shared<KVServiceImpl>(cache, identityAeroService);

    ServerBuilder builder;
    builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
    builder.RegisterService(service.get());
    std::unique_ptr <Server> server(builder.BuildAndStart());
    std::cout << "Server listening on " << server_address << std::endl;


    server->Wait();
}

int main(int argc, char **argv) {
    // Expect only arg: --db_path=path/to/route_guide_db.json.
    // std::string db = routeguide::GetDbFileContent(argc, argv);

    std::set_terminate(mm::Util::customTerminate);
    SignalHandler::installHandlers();
    std::string appName = "advanced_cache_server";
    mm::Util::configureLogging(appName, argv);

    auto configService = std::make_shared<mm::ConfigService>(
            appName + std::string(".properties"),
            "common.properties");
//    LOG(ERROR) << appName <<" starting with properties "<< propertyFileName <<
//               std::endl<<" original arguments : argc "<< argc << " argv : " <<  *argv;

//    RunPopulationService();
    auto dataProvider = std::make_shared <mm::DataProvider>();

    std::string host = "127.0.0.1";
    host = "aerospikedb";//this is the hostname in docker-compose
    int port = 3000;
    const char *lua_user_path = nullptr;
    auto aerospikeDriver = std::make_shared<AerospikeDriver>(host, port, lua_user_path);

    auto dataPopulateService = std::make_shared<mm::DataPopulateService>(
            dataProvider,
            aerospikeDriver,
            configService->get("aerospike_namespace"),
            configService->get("aerospike_set"));

    if (StringUtil::equalsIgnoreCase(configService->get("populate_aerospike"), "true")) {
        //dataPopulateService shouldnt be created after if, it will segfault
        dataPopulateService->startPopulationThread();
    }

    RunServer(aerospikeDriver, configService->get("aerospike_namespace"),
              configService->get("aerospike_set"));

    return 0;
}
