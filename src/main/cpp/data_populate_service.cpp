
#include "data_populate_service.h"
#include "identity_aerospike_service.h"
#include "util.h"
#include <thread>

mm::DataPopulateService::DataPopulateService(
        std::shared_ptr <DataProvider> dataProvider,
        std::shared_ptr <AerospikeDriver> aerospikeDriver,
        std::string namespaceName,
        std::string setName) {
    this->dataProvider = dataProvider;
    this->aerospikeDriver = aerospikeDriver;
    this->namespaceName = namespaceName;
    this->setName = setName;
}

mm::DataPopulateService::~DataPopulateService() {

}

void mm::DataPopulateService::populateAersoSpike() {

    while (true) {
        //create the sample random data
        auto allData = dataProvider->createExampleResponse(100);

        for (std::shared_ptr<::kv::DenormalizedData> data : *allData) {
            std::string output;
            bool result = data->SerializeToString(&output);
            if (result) {
                aerospikeDriver->set_cache(namespaceName, setName, data->connected_id(),
                                           IdentityAerospikeService::blobData, output, 3600);
            } else {
                std::cerr << "result of serialization is 0"<<std::endl;
            }
        }
        VLOG(1) << "inserted " << allData->size()<< " data to kv";
        mm::Util::sleepMillis(100);
        //insert into aerospike
    }
}

void mm::DataPopulateService::startPopulationThread() {
    std::thread runAllThreads (&mm::DataPopulateService::populateAersoSpike, this);
    runAllThreads.detach ();
}
