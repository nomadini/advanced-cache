// Copyright 2015 gRPC authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto2";

package types;

option go_package = "github.com/MediaMath/identity-types/common/types";
option java_multiple_files = true;
option java_package = "com.mediamath.identity.common.types";
option java_outer_classname = "CommonTypes";

// IDType defines the vertex type
// IMPORTANT: we are (mostly) reliant on the names
// having unique first chars. see the types file for string()
enum IDType {
    UNKNOWN = 0;
    CID = 1;
    COOKIE = 2;
    ANDROID = 3;
    IDFA = 4;
    SDK = 5;
    EMAIL_HASH = 6;
    OTHER = 7;
    LRID = 8;
    HOUSEHOLD = 9;
}

// EdgeType defines the types of links between vertices
// IMPORTANT: we are (mostly) reliant on the names
// having unique first chars. see the types file for string()
enum EdgeType {
    RELATED = 0;
    PROBABILISTIC = 1;
    DETERMINISTIC = 2;
}

// EdgeRelationship defines the relationship between Vertices that an Edge links
enum EdgeRelationship {
    CONNECTS_TO = 0;
    MAPS_TO = 1;
}

// Status defines 
enum Status {
    ALIVE = 0;
    DEAD = 1;
}

message Vertex {
    // Actual ID of the device / individual / household, such as CID, DeviceID etc., usually in UUID format.
    optional string entity_id = 1;
    // Type of the entity_id, CID / DeviceID / LiveRamp ID / HouseHold ID etc.
    optional IDType type = 2;
    //Domain
    optional string source = 3;
    // Data vendor, such as LiveRamp
    optional string vendor_name = 4;
    // Is this third party cookie id?
    optional bool is_3p = 5;
    // Browser and / or OS type
    optional string browser_os = 6;
    // Type of the device, 1 (mobile) or 2 (desktop). This is referenced from Oracle CrossWise data manual.
    optional uint32 device_type = 7;
    // Country of the origin of this ID
    optional string country = 8;
    // Hash function used to hash original ID value into vertex_id
    optional string hash_func = 9;
    // Timestamp when this vertex was created
    optional uint64 ts_created = 10;
    // Timestamp when this vertex was updated
    optional uint64 ts_updated = 11;
    // Timestamp when ID represented by this vertex was last seen
    optional uint64 ts_last_seen = 12;
    // Count of how many times this ID was seen
    optional uint64 seen_count = 13;
    optional Status status = 14;
    // Whether device ID represented by this vertex is opted out
    optional bool is_opted_out = 15;
}

message Edge {
    // Type of the edge
    optional EdgeType type = 1;
    // Relationship between 2 vertices that this edge links
    optional EdgeRelationship relationship = 2;
    // Connection confidence provided by data vendor
    optional float vendor_confidence = 3;
    // Connection confidence calculated by MediaMath
    optional float mm_confidence = 4;
    // Name of the vendor providing this connection
    optional string vendor_name = 5;
    // Timestamp when this edge was created
    optional uint64 ts_created = 6;
    // Timestamp when this edge was attached to vertices
    optional uint64 ts_attached = 7;
    optional Status status = 8;
}
