
### How to build this project?

This project depends on prometheus-cpp and grpc

How to build prometheus-cpp:
  follow cmake instructions on https://github.com/jupp0r/prometheus-cpp Readme.md but instead of last 
  instruction do "make install" to deploy lib and headers in standard directories

How to build or install protobuf-compiler-grpc on ubuntu:
  apt-get install protobuf-compiler-grpc  or run the below command

  apt-get install build-essential autoconf libtool && apt-get install libgflags-dev libgtest-dev && apt-get install clang libc++-dev && git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc && cd grpc && git submodule update --init && make && make install


How to compile all protobufs?

you need to run both commands
/home/advanced-cache/src/main/proto# protoc -I . --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` kv.proto
/home/advanced-cache/src/main/proto# protoc -I . --cpp_out=. kv.proto


root@abdea6811696:/home/advanced-cache/src/main/proto# protoc -I . --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` identity-types/common/api/core.proto
root@abdea6811696:/home/advanced-cache/src/main/proto# protoc -I . --cpp_o
ut=. identity-types/common/api/core.proto

root@abdea6811696:/home/advanced-cache/src/main/proto# protoc -I . --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` identity-types/common/types/core.proto
root@abdea6811696:/home/advanced-cache/src/main/proto# protoc -I . --cpp_o
ut=. identity-types/common/types/core.proto

How to find where libraries are installed ?
ldconfig -p

#### how to install g++9

    apt install software-properties-common
    add-apt-repository ppa:ubuntu-toolchain-r/test
    apt install gcc-9 g++-9
    update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9 --slave /usr/bin/gcov gcov /usr/bin/gcov-9
    gcc --version
### How to install tbb
    wget https://github.com/intel/tbb/archive/v2020.1.tar.gz && tar -xvf v2020.1.tar.gz  && cd tbb-2020.1/ && make
    mv /tmp/tbb-2020.1/ /home/advanced-cache/


### How to install cpp_redis
    https://github.com/cpp-redis/cpp_redis/wiki/Mac-&-Linux-Install

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slav
#### How to install Poco and other libraries?
    wget https://github.com/pocoproject/poco/archive/poco-1.10.1-release.tar.gz && mv poco-1.10.1-release.tar.gz /tmp/ && cd /tmp && tar -xvf /tmp/poco-1.10.1-release.tar.gz && cmake . &&  make && make install
    apt-get install libboost-all-dev
    
### Hot to install aerospike client library
    apt-get install libc6-dev libssl-dev autoconf automake libtool g++
    apt-get --assume-yes install libev-dev
    git clone https://github.com/aerospike/aerospike-client-c && cd aerospike-client-c/ && git checkout tags/4.6.13 -b 4.6.13 && git submodule update --init && make EVENT_LIB=libev && make install
     
#### How to run this app?
    docker-compose up
    docker  exec -it 0e033a686716 /bin/bash
    cd /home/advanced-cache
    cmake . -G Ninja -DCMAKE_MAKE_PROGRAM=ninja && ninja

### TO Dos for this project
    write a thread that inserts synthetic data into aerospike (done)
    test that client-server can communicate properly (done)
    use config based values instead of hardcoded values for dev and prod env (almost done)
    enable dynamic logging
    create a nice dashboard and sniffing functionality for debugging
    add metrics (mostly prom counters to the code) 
    write the client for redis and cassandra (for future modified version of the project)
    add consul and consisten hashing logic for load balancing and service discovery
    
    add short-term cache(first time seeing devices that we return empty result if not 
    found in cache, but we initiate the query and populate the cache if the result is there for second
    requests from the deivces, then if hit 2 or n times, we put it in long-term cache)
    
    this way we always return empty result fast for devices that we only see once.

