apt-get  -y update && apt-get install  -f -y  build-essential --fix-missing
apt-get  -y update && apt-get install  -f -y  vim --fix-missing
apt-get  -y update && apt-get install  -f -y  cmake --fix-missing
apt-get  -y update && apt-get install  -f -y  curl --fix-missing
apt-get  -y update && apt-get install  -f -y   libcurl4-openssl-dev --fix-missing
apt-get  -y update && apt-get install  -f -y  software-properties-common --fix-missing
apt-get  -y update && apt-get install  -f -y  wget --fix-missing
apt-get  -y update && install -f -y wget curl
apt-get  -y update && apt-get install  -f -y  autotools-dev --fix-missing
apt-get  -y update && apt-get install  -f -y  ninja-build --fix-missing
apt-get  -y update && apt-get install  -f -y  automake --fix-missing

apt-get -y install libmysqlcppconn-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   g++ --fix-missing
apt-get  -y update && apt-get install  -f -y   libunwind-dev --fix-missing
apt-get  -y update && apt-get install  -f -y  gdb --fix-missing

apt-get  -y update && apt-get install  -f -y  llvm-7 --fix-missing
apt-get  -y update && apt-get install  -f -y  binutils-gold --fix-missing
apt-get  -y update && apt-get install  -f -y  clang --fix-missing
apt-get  -y update && apt-get install  -f -y  ccache --fix-missing

apt-get  -y update && apt-get install  -f -y  git --fix-missing
apt-get  -y update && apt-get install  -f -y  libsnappy-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   libgflags-dev libgoogle-glog0v5 --fix-missing
apt-get  -y update && apt-get install  -f -y   libgoogle-glog0v5 --fix-missing
apt-get  -y update && apt-get install  -f -y  python3-pip --fix-missing
apt-get  -y update && apt-get install  -f -y  libgoogle-glog-dev --fix-missing

pip3 install conan
apt -f -y install zlib1g-dev
apt-get -f -y install protobuf-compiler-grpc
apt-get -f -y install libprotobuf-dev

apt-get  -y update && apt-get install  -f -y   libssl-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   libuv1-dev --fix-missing
apt-get  -y update && apt-get install  -f -y   python3-setuptools --fix-missing
apt-get  -y update && apt-get install  -f -y   libshogun16 --fix-missing
apt-get  -y update && apt-get install  -f -y   libshogun-dev --fix-missing

apt-get  -y update && apt-get install  -f -y  openjdk-8-jdk --fix-missing
apt-get  -y update &&  apt-get install  -f -y   maven --fix-missing

apt-get update && apt-get install libgtest-dev && apt-get install clang libc++-dev && cd /tmp/ && git clone -b $(curl -L https://grpc.io/release) https://github.com/grpc/grpc && cd grpc && git submodule update --init && make && make install
